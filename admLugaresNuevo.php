<?php require_once("master.php"); cabecera(); ?>
    <div id='wrapper' class='toggled'>
        <?php sideBar(); ?>

        <!-- Page Content -->
        <div id='page-content-wrapper'>
            <?php topBar(); ?>
                <div class='container'>
                    <div class="row">
                        <div class="col-md-12 noP">
                            <form>
                                <div class="col-md-12 text-center vistaLogin">
                                    <div class="col-md-12 noP">
                                        <div class="col-md-12">
                                            <h2>Agregar proveedores <img class="imgTtitulo" src="img/lugares.png" alt=""></h2> <br>
                                        </div>
                                    </div>

                                    <div class="col-md-8 col-md-offset-2 noP">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Nombre del proveedor (*)">
                                            </div>

                                            <div class="form-group">
                                                <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Teléfono del proveedor (*)">
                                            </div>

                                            <div class="form-group">
                                                <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Correo del proveedor (*)">
                                            </div>

                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Tipo de producto / servicio (*)">
                                            </div>

                                            <div class="form-group">
                                                <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Ubicación del proveedor (*)">
                                            </div>

                                            <div class="form-group">
                                                <select class="form-control loginInput" name="">
                                                    <option value="">Categoria</option>
                                                    <option value="">categoria 1</option>
                                                    <option value="">categoria 2</option>
                                                    <option value="">categoria 3</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                ¿ No está la categoria ?
                                                <a href="admCategoriasNuevo.php" class="btn btn-info" type="button" name="button">Agregar categoria</a>
                                            </div>

                                        </div>

                                        <div class="col-md-12">
                                            <p class="text-danger">(*) Campos obligatorios</p>
                                            <a class="btn btn-success" href="#">Guardar</a>
                                            <a class="btn btn-warning" href="admLugares.php">Volver</a>
                                        </div>

                                    </div>

                                </div>
                            </form>
                        </div>

                    </div>
                </div><!--container-->
        </div>
        <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->


    <script>
        $(document).ready(function () {
            // Seccion active (MENU)
            $(document).ready(function () {
                $('#publicid').addClass('activo');
                $('#publicidli').addClass('activoli');
            });

        });
    </script>

<?php footer(); ?>
