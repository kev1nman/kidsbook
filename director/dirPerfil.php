<?php require_once("master.php"); cabecera(); ?>
    <div id='wrapper' class='toggled'>
        <?php sideBar(); ?>

        <!-- Page Content -->
        <div id='page-content-wrapper'>
            <?php topBar(); ?>
                <div class='container'>
                    <div class="col-md-12 noP">
                        <form>
                            <div class="col-md-12 text-center vistaLogin">
                                <div class="col-md-12 noP">
                                    <div class="col-md-12">
                                        <h2>Mi perfil <img class="imgTtitulo" src="../img/koala.png" alt=""></h2> <br>
                                    </div>
                                </div>
                                <p class="text-danger">(*) Campos obligatorios</p>

                                <div class="col-md-8 col-md-offset-2 noP">
                                    <div class="col-md-12">

                                        <div class="col-md-12">
                                            <div class="form-group borderCuadro">
                                                <label for="ejemplo_archivo_1">Agregar logo del Colegio</label>
                                                <input type="file" id="ejemplo_archivo_1">
                                                <p class="help-block">La imagen debe ser cuadrada (1:1) y en formatos como jpg/png/gif.</p>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Nombre del director (*)">
                                        </div>

                                        <div class="form-group">
                                            <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Persona de contácto numero (*)">
                                        </div>

                                        <div class="form-group">
                                            <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Correo de electrónico alternativo (*)">
                                        </div>

                                        <div class="form-group">
                                            <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Pais (*)">
                                        </div>

                                        <div class="form-group">
                                            <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Estado(*)">
                                        </div>

                                        <div class="form-group">
                                            <textarea type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Dirección (*)"></textarea>
                                        </div>


                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Nombre del colegio (*)">
                                        </div>

                                        <div class="form-group">
                                            <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="RIF (*)">
                                        </div>

                                        <div class="form-group">
                                            <input type="email" class="form-control loginInput" id="exampleInputEmail1" placeholder="Teléfono del colegio (*)">
                                        </div>

                                        <div class="form-group">
                                            <h3>Mapa</h3>
                                        </div>
                                    </div>


                                </div>

                                <div class="col-md-12">
                                    <a class="btn btn-success" href="#">Guardar</a>
                                    <a class="btn btn-warning" href="dirEstadisticas.php">Volver</a>
                                </div>


                            </div>

                        </form>
                    </div>
                </div><!--container-->
        </div>
        <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->


    <script>
        $(document).ready(function () {
            // Seccion active (MENU)
            $(document).ready(function () {
                $('#perfil').addClass('activo');
                $('#perfilli').addClass('activoli');
            });

        });
    </script>

<?php footer(); ?>
