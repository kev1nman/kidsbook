<?php require_once("master.php"); cabecera(); ?>
    <div id='wrapper' class='toggled'>
        <?php sideBar(); ?>

        <!-- Page Content -->
        <div id='page-content-wrapper'>
            <?php topBar(); ?>
                <div class='container-fluid'>
                    <div class='row'>
                        <div class='col-md-12 noP fila'>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-7 noP">
                                    <h2 class='titulo2'>Representante</h2>
                                </div>
                                <div class="col-xs-5 noP">
                                    <img class="imgRecuadro" src="../img/representantes2.png" alt="">
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-12 recuadroInfo sombra noP">
                                    <img class="imgRecuadro" src="../img/ninos.png" alt="">
                                    <h3 class="titulo3">
                                        Niños <br>
                                        <small>542</small>
                                    </h3>
                                    <img class="imgRecuadro2" src="../img/mundo.png" alt="">
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-12 recuadroInfo sombra noP">
                                    <img class="imgRecuadro" src="../img/representantes.png" alt="">
                                    <h3 class="titulo3">
                                        Representantes <br>
                                        <small>854</small>
                                    </h3>
                                    <img class="imgRecuadro2" src="../img/mundo.png" alt="">
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-12 recuadroInfo sombra noP">
                                    <img class="imgRecuadro" src="../img/Maestros.png" alt="">
                                    <h3 class="titulo3">
                                        Maestros <br>
                                        <small>19</small>
                                    </h3>
                                    <img class="imgRecuadro2" src="../img/mundo.png" alt="">
                                </div>
                            </div>

                        </div>

                        <div class="col-xs-12 noP">
                            <div class="panel panel-default">
                                <!-- Default panel contents -->
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-md-12 addNewBtn">
                                            <a class="btn btn-success"  href="dirRepresentanteNuevo.php">Agregar representante</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <button type="button" class="btn btn-default disabled" name="button"><span class="glyphicon glyphicon-trash"></span> Borrar seleccionados</button>
                                </div>

                                <div class="table-responsive">
                                    <!-- Table -->
                                    <table id="representantes" class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th>Nombre</th>
                                                <th>Parentezco</th>
                                                <th>Teléfono</th>
                                                <th>Correo</th>
                                                <th>Acciones</th>
                                            </tr>

                                        </thead>
                                        <tr>
                                            <td class="checking"><input type="checkbox" name="" value=""></td>
                                            <td class="checking"><span class="icon-star-full text-warning starColor"></span></td>
                                            <td> Jhan Castillo</td>
                                            <td>Padre</td>
                                            <td>0414-7170083</td>
                                            <td>Representante@correo.com</td>
                                            <td>

                                                <span class="glyphicon glyphicon-trash iconTable"></span>
                                                <a class="text-warning" href="dirRepresentantePerfil.php"><span class="glyphicon glyphicon-eye-open iconTable"></span></a>
                                                <span class="glyphicon glyphicon-plus iconTable"></span>
                                                <span class="glyphicon glyphicon-user iconTable"></span>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="checking"><input type="checkbox" name="" value=""></td>
                                            <td class="checking"><span class="icon-star-full text-warning starColor"></span></td>
                                            <td>Yesica Castillo</td>
                                            <td>Madre</td>
                                            <td>0414-7170083</td>
                                            <td>Representante@correo.com</td>
                                            <td>

                                                <span class="glyphicon glyphicon-trash iconTable"></span>
                                                <a class="text-warning" href="dirRepresentantePerfil.php"><span class="glyphicon glyphicon-eye-open iconTable"></span></a>
                                                <span class="glyphicon glyphicon-plus iconTable"></span>
                                                <span class="glyphicon glyphicon-user iconTable"></span>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="checking"><input type="checkbox" name="" value=""></td>
                                            <td class="checking"><span class="icon-star-full text-warning starColor"></span></td>
                                            <td>Yesica Castillo</td>
                                            <td>Madre</td>
                                            <td>0414-7170083</td>
                                            <td>Representante@correo.com</td>
                                            <td>

                                                <span class="glyphicon glyphicon-trash iconTable"></span>
                                                <a class="text-warning" href="dirRepresentantePerfil.php"><span class="glyphicon glyphicon-eye-open iconTable"></span></a>
                                                <span class="glyphicon glyphicon-plus iconTable"></span>
                                                <span class="glyphicon glyphicon-user iconTable"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="checking"><input type="checkbox" name="" value=""></td>
                                            <td class="checking"><span class="icon-star-full text-warning starColor"></span></td>
                                           <td>Ana Padilla</td>
                                            <td>Madre</td>
                                            <td>0414-7170083</td>
                                            <td>Representante@correo.com</td>
                                            <td>
                                                <span class="glyphicon glyphicon-trash iconTable"></span>
                                                <a class="text-warning" href="dirRepresentantePerfil.php"><span class="glyphicon glyphicon-eye-open iconTable"></span></a>
                                                <span class="glyphicon glyphicon-plus iconTable"></span>
                                                <span class="glyphicon glyphicon-user iconTable"></span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="panel-body">
                                    <button type="button" class="btn btn-default disabled" name="button"><span class="glyphicon glyphicon-trash"></span> Borrar seleccionados</button>
                                </div>
                            </div>
                        </div>


                    </div>
                </div><!--container-->
        </div>
        <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->


    <script>
        $(document).ready(function () {
            // Seccion active (MENU)
            $(document).ready(function () {
                $('#representante').addClass('activo');
                $('#representanteli').addClass('activoli');
            });

            $(document).ready(function() {
                $('#representantes').DataTable();
            } );

        });
    </script>

<?php footer(); ?>
