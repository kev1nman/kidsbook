<?php require_once("master.php"); cabecera(); ?>
    <div id='wrapper' class='toggled'>
        <?php sideBar(); ?>

        <!-- Page Content -->
        <div id='page-content-wrapper'>
            <?php topBar(); ?>
                <div class='container'>
                    <div class="col-md-12 noP">
                        <form>
                            <div class="col-md-12 text-left vistaLogin">
                                <div class="col-md-8 col-xs-offset-3 noP">
                                    <div class="col-md-12">
                                        <h2>Agregar nuevo representante <img class="imgTtitulo" src="../img/representantes2.png" alt=""></h2> <br>
                                    </div>

                                </div>

                                <div class="col-md-10 col-md-offset-1 noP">
                                    <div class="col-m-8 col-md-offset-3">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <select class="form-control">
                                                    <option>Lista de niños en Nómina</option>
                                                    <option>Jhan</option>
                                                    <option>Mario</option>
                                                    <option>Juana</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <a class="btn btn-info" href="#">Agregar otro familiar</a>
                                            </div>
                                        </div>
                                    </div>



                                    <div class="col-md-12">
                                        <p class="text-danger text-center">(*) Campos obligatorios</p>
                                    </div>


                                    <div class="col-md-6">

                                        <div class="text-center ficha">
                                            <h4>Representante 1</h4>
                                        </div>

                                        <div class="form-group">
                                            <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Nombre y Apellido (*)">
                                        </div>

                                        <div class="form-group">
                                            <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Teléfono (*)">
                                        </div>

                                        <div class="form-group">
                                            <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Parentesco (*)">
                                        </div>

                                        <div class="form-group">
                                            <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Correo Electrónico (*)">
                                        </div>

                                        <div class="form-group">
                                            <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Dirección">
                                        </div>

                                    </div>

                                    <div class="col-md-6">

                                        <div class="text-center  ficha">
                                            <h4>Representante 2</h4>
                                        </div>

                                        <div class="form-group">
                                            <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Nombre y Apellido (*)">
                                        </div>

                                        <div class="form-group">
                                            <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Teléfono (*)">
                                        </div>

                                        <div class="form-group">
                                            <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Parentesco (*)">
                                        </div>

                                        <div class="form-group">
                                            <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Correo Electrónico  (*)">
                                        </div>

                                        <div class="form-group">
                                            <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Dirección " title="Si es la misma que la anterior obvie este campo">
                                        </div>


                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Apellido del Representado" disabled>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder=" Nombre del Representado" disabled>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Nivel" disabled>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-10 col-md-offset-2 text-center">
                                    <div class="col-md-6">
                                        <h3><img class="imgTtitulo" src="../img/ninos2.png" alt="">Cantidad de representados</h3>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="botonesNiveles2">
                                            <div class="total total3 bg-success">
                                                <h3>0</h3>
                                            </div>
                                        </div>
                                    </div>




                                </div>

                                <div class="col-md-12 text-center">
                                    <a class="btn btn-success" href="#">Guardar</a>
                                    <a class="btn btn-warning" href="dirRepresentantes.php">Volver</a>
                            </div>

                        </form>
                    </div>
                </div><!--container-->
        </div>
        <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->


    <script>
        $(document).ready(function () {
            // Seccion active (MENU)
            $(document).ready(function () {
                $('#representante').addClass('activo');
                $('#representanteli').addClass('activoli');
            });

        });
    </script>

<?php footer(); ?>
