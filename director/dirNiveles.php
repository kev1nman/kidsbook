<?php require_once("master.php"); cabecera(); ?>
    <div id='wrapper' class='toggled'>
        <?php sideBar(); ?>

        <!-- Page Content -->
        <div id='page-content-wrapper'>
            <?php topBar(); ?>
                <div class='container-fluid'>
                    <div class='row'>
                        <div class='col-md-12 noP fila'>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-7 noP">
                                    <h2 class='titulo2'>Niveles</h2>
                                </div>
                                <div class="col-xs-5 noP">
                                    <img class="imgRecuadro" src="../img/niveles.png" alt="">
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-12 recuadroInfo sombra noP">
                                    <img class="imgRecuadro" src="../img/ninos.png" alt="">
                                    <h3 class="titulo3">
                                        Niños <br>
                                        <small>542</small>
                                    </h3>
                                    <img class="imgRecuadro2" src="../img/mundo.png" alt="">
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-12 recuadroInfo sombra noP">
                                    <img class="imgRecuadro" src="../img/representantes.png" alt="">
                                    <h3 class="titulo3">
                                        Representantes <br>
                                        <small>854</small>
                                    </h3>
                                    <img class="imgRecuadro2" src="../img/mundo.png" alt="">
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-12 recuadroInfo sombra noP">
                                    <img class="imgRecuadro" src="../img/Maestros.png" alt="">
                                    <h3 class="titulo3">
                                        Maestros <br>
                                        <small>19</small>
                                    </h3>
                                    <img class="imgRecuadro2" src="../img/mundo.png" alt="">
                                </div>
                            </div>

                        </div>

                        <div class="col-md-8 noP">
                            <div class="panel panel-default">
                                <!-- Default panel contents -->
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-md-12 addNewBtn">
                                            <a class="btn btn-success"  href="dirNivelesNuevo.php">Agregar nivel</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <button type="button" class="btn btn-default disabled" name="button"><span class="glyphicon glyphicon-trash"></span> Borrar seleccionados</button>
                                </div>

                                <div class="table-responsive">
                                    <!-- Table -->
                                    <table id="profe" class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th>Nombre</th>
                                                <th>Seccion</th>
                                                <th>Aula</th>
                                                <th>Nº de Alumnos</th>
                                                <th>Nº de Maestros</th>
                                                <th>Acciones</th>
                                            </tr>

                                        </thead>
                                        <tr>
                                            <td class="checking"><input type="checkbox" name="" value=""></td>
                                            <td class="checking"><span class="icon-star-full text-warning starColor"></span></td>
                                            <td> Ositos</td>
                                            <td>Única</td>
                                            <td>A1</td>
                                            <td><strong class="total bg-success">0</strong></td>
                                            <td>01</td>
                                            <td>

                                                <span class="glyphicon glyphicon-trash iconTable"></span>
                                                <a class="text-warning" href="dirNivelesPerfil.php"><span class="glyphicon glyphicon-eye-open iconTable"></span></a>

                                            </td>
                                        </tr>


                                    </table>
                                </div>
                                <div class="panel-body">
                                    <button type="button" class="btn btn-default disabled" name="button"><span class="glyphicon glyphicon-trash"></span> Borrar seleccionados</button>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 botonesNiveles">
                            <div class="total bg-success">Cupos disponibles en aula</div>
                            <div class="total bg-warning">Aula casi llena</div>
                            <div class="total bg-danger">Aula llena</div>

                            <br><br>

                            Total niveles <strong class="total bg-success">02</strong> <br> <br>
                            Total aulas en sistema <strong class="total bg-success">02</strong>
                        </div>


                    </div>
                </div><!--container-->
        </div>
        <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->


    <script>
        $(document).ready(function () {
            // Seccion active (MENU)
            $(document).ready(function () {
                $('#niveles').addClass('activo');
                $('#nivelesli').addClass('activoli');
            });

            $(document).ready(function() {
                $('#profe').DataTable();
            } );

        });
    </script>

<?php footer(); ?>
