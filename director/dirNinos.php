<?php require_once("master.php"); cabecera(); ?>
    <div id='wrapper' class='toggled'>
        <?php sideBar(); ?>

        <!-- Page Content -->
        <div id='page-content-wrapper'>
            <?php topBar(); ?>
                <div class='container-fluid'>
                    <div class='row'>
                        <div class='col-md-12 noP fila'>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-7 noP">
                                    <h2 class='titulo2'>Niños</h2>
                                </div>
                                <div class="col-xs-5 noP">
                                    <img class="imgRecuadro" src="../img/ninos2.png" alt="">
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-12 recuadroInfo sombra noP">
                                    <img class="imgRecuadro" src="../img/ninos.png" alt="">
                                    <h3 class="titulo3">
                                        Niños <br>
                                        <small>542</small>
                                    </h3>
                                    <img class="imgRecuadro2" src="../img/mundo.png" alt="">
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-12 recuadroInfo sombra noP">
                                    <img class="imgRecuadro" src="../img/representantes.png" alt="">
                                    <h3 class="titulo3">
                                        Representantes <br>
                                        <small>854</small>
                                    </h3>
                                    <img class="imgRecuadro2" src="../img/mundo.png" alt="">
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-12 recuadroInfo sombra noP">
                                    <img class="imgRecuadro" src="../img/Maestros.png" alt="">
                                    <h3 class="titulo3">
                                        Maestros <br>
                                        <small>20</small>
                                    </h3>
                                    <img class="imgRecuadro2" src="../img/mundo.png" alt="">
                                </div>
                            </div>

                        </div>

                        <div class="col-xs-12 noP">
                            <div class="panel panel-default">
                                <!-- Default panel contents -->
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-md-12 addNewBtn">
                                            <a class="btn btn-success"  href="dirNinosNuevo.php">Agregar niño</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <button type="button" class="btn btn-default disabled" name="button"><span class="glyphicon glyphicon-trash"></span> Borrar seleccionados</button>
                                </div>

                                <div class="table-responsive">
                                    <!-- Table -->
                                    <table id="representantes" class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th>Nombre</th>
                                                <th>Edad</th>
                                                <th>Sexo</th>
                                                <th>Representante 1</th>
                                                <th>Representante 2</th>
                                                <th>Acciones</th>
                                            </tr>

                                        </thead>
                                        <tr>
                                            <td class="checking"><input type="checkbox" name="" value=""></td>
                                            <td class="checking"><img class="img-circle imgNino" src="../img/colegios.png"></td>
                                            <td> Jhan Castillo</td>
                                            <td>4 años</td>
                                            <td><img class="imgNino2" src="../img/nino.png"></td></td>
                                            <td>Maria Castillo</td>
                                            <td>Jose Castillo</td>
                                            <td>

                                                <span class="glyphicon glyphicon-trash iconTable"></span>
                                                <a class="text-warning" href="dirNinosPerfil.php"><span class="glyphicon glyphicon-eye-open iconTable"></span></a>
                                                <span class="glyphicon glyphicon-plus iconTable"></span>
                                                <span class="glyphicon glyphicon-user iconTable"></span>

                                            </td>
                                        </tr>

                                    </table>
                                </div>
                                <div class="panel-body">
                                    <button type="button" class="btn btn-default disabled" name="button"><span class="glyphicon glyphicon-trash"></span> Borrar seleccionados</button>
                                </div>
                            </div>
                        </div>


                    </div>
                </div><!--container-->
        </div>
        <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->


    <script>
        $(document).ready(function () {
            // Seccion active (MENU)
            $(document).ready(function () {
                $('#ninos').addClass('activo');
                $('#ninosli').addClass('activoli');
            });

            $(document).ready(function() {
                $('#representantes').DataTable();
            } );

        });
    </script>

<?php footer(); ?>
