<?php require_once("master.php"); cabecera(); ?>
    <div id='wrapper' class='toggled'>
        <?php sideBar(); ?>

        <!-- Page Content -->
        <div id='page-content-wrapper'>
            <?php topBar(); ?>
                <div class='container'>
                    <div class="col-md-12 noP">
                        <form>
                            <div class="col-md-12 text-center vistaLogin">
                                <div class="col-md-12 noP">
                                    <div class="col-md-12">
                                        <h2>Agregar niño <img class="imgTtitulo" src="../img/ninos2.png" alt=""></h2> <br>
                                    </div>
                                </div>
                                <p class="text-danger">(*) Campos obligatorios</p>

                                <div class="col-md-8 col-md-offset-2 noP">
                                    <div class="col-md-12 noP">
                                       
                                        <div class="col-md-8">
                                            <div class="form-group borderCuadro">
                                                <label for="ejemplo_archivo_1">Agregar logo del niño</label>
                                                <input type="file" id="ejemplo_archivo_1">
                                                <p class="help-block">La imagen debe ser cuadrada (1:1) y en formatos como jpg/png/gif.</p>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="input-group">
                                                <h5>Sexo:</h5>
                                                <label for="tema1"><img class="imgNino2" src="../img/nino.png"></label><input name="tema" id="tema1" type="radio" class="radioInput" placeholder="">
                                                <label for="tema2"><img class="imgNino2" src="../img/nina.png"></label><input name="tema" id="tema2" type="radio" class="radioInput" placeholder="">
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-12 noP">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Nombre del niño (*)">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Apellido del niño (*)">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Fecha de nacimiento (*)">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <select class="form-control loginInput" name="">
                                                 <option value="">Nivel (*)</option>
                                                 <option value="">1</option>
                                                 <option value="">2</option>
                                                 <option value="">3</option>
                                            </select>
                                            </div>
                                        </div>
                                    </div>





                                    <div class='col-md-12' >
                                        <div class='col-md-3 text-center'>
                                            <span>Usa pañal:</span>
                                            <div class='form-group radiosbebe'>
                                                <label for="panalSi">SI </label><input id="panalSi" type="radio" name="usaPanal">
                                                <input id="panalNo" type="radio" name="usaPanal"> <label for="panalNo"> NO</label>
                                            </div>
                                        </div>

                                        <div class='col-md-3 text-center'>
                                            <span>Va solo al baño:</span>
                                            <div class='form-group radiosbebe'>
                                                <label for="banoSi">SI </label><input id="banoSi" type="radio" name="usaBano">
                                                <input id="banoNo" type="radio" name="usaBano"> <label for="banoNo"> NO</label>
                                            </div>
                                        </div>

                                        <div class='col-md-3 text-center'>
                                            <span>Toma Tetero:</span>
                                            <div class='form-group radiosbebe'>
                                                <label for="teteroSi">SI </label><input id="teteroSi" type="radio" name="usaTetero">
                                                <input id="teteroNo" type="radio" name="usaTetero"> <label for="teteroNo"> NO</label>
                                            </div>
                                        </div>

                                        <div class='col-md-3 text-center'>
                                            <span>Come solo:</span>
                                            <div class='form-group radiosbebe'>
                                                <label for="comeSi">SI </label><input id="comeSi" type="radio" name="usaCome">
                                                <input id="comeNo" type="radio" name="usaCome"> <label for="comeNo"> NO</label>
                                            </div>
                                        </div>

                                    </div>



                                </div>

                                <div class="col-md-12">
                                    <a class="btn btn-success" href="#">Guardar</a>
                                    <a class="btn btn-warning" href="dirNinos.php">Volver</a>
                                </div>


                            </div>

                        </form>
                    </div>
                </div><!--container-->
        </div>
        <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->


    <script>
        $(document).ready(function () {
            // Seccion active (MENU)
            $(document).ready(function () {
                $('#ninos').addClass('activo');
                $('#ninosli').addClass('activoli');
            });

        });
    </script>

<?php footer(); ?>
