<?php require_once("master.php"); cabecera(); ?>
    <div id='wrapper' class='toggled'>
        <?php sideBar(); ?>

        <!-- Page Content -->
        <div id='page-content-wrapper'>
            <?php topBar(); ?>
                <div class='container-fluid'>
                    <div class='row'>
                        <div class='col-md-12 noP fila'>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-7 noP">
                                    <h2 class='titulo2'>Estadísticas</h2>
                                </div>
                                <div class="col-xs-5 noP">
                                    <img class="imgTtitulo" src="../img/estadisticas.png" alt="">
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-12 recuadroInfo sombra noP">
                                    <img class="imgRecuadro" src="../img/ninos.png" alt="">
                                    <h3 class="titulo3">
                                        Niños <br>
                                        <small>542</small>
                                    </h3>
                                    <img class="imgRecuadro2" src="../img/mundo.png" alt="">
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-12 recuadroInfo sombra noP">
                                    <img class="imgRecuadro" src="../img/representantes.png" alt="">
                                    <h3 class="titulo3">
                                        Representantes <br>
                                        <small>854</small>
                                    </h3>
                                    <img class="imgRecuadro2" src="../img/mundo.png" alt="">
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-12 recuadroInfo sombra noP">
                                    <img class="imgRecuadro" src="../img/colegios.png" alt="">
                                    <h3 class="titulo3">
                                        Colegios <br>
                                        <small>19</small>
                                    </h3>
                                    <img class="imgRecuadro2" src="../img/mundo.png" alt="">
                                </div>
                            </div>

                        </div>

                        <div class='col-xs-12 noP fila'>
                            <div class="col-md-6">
                                <div id="grafico2" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                            </div>
                            <div class="col-md-4">
                                <div id="grafico1" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                            </div>

                            <div class="col-md-2">
                                <div class="col-xs-12 masUsuarios sombra noP">
                                    <div class="col-sm-4 col-lg-12 imagenMasUsuarios">
                                        <img src="../img/masNinos.png" alt="">
                                    </div>

                                    <div class="col-sm-8 col-md-12 datosMasUsuarios">
                                        <h2 class="text-porcentaje text-center">67%
                                        <small class="text-porcentaje">Incremento de inscritos</small></h2>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class='col-xs-12 fila'>
                            <div class="col-md-4">
                                <div id="grafico3" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                            </div>
                            <div class="col-md-4">
                                <div id="grafico4" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                            </div>
                            <div class="col-xs-12 col-md-4 usuariosON sombra noP">
                                <div class="col-xs-12 noP">
                                    <div class="col-xs-12">
                                        <h4>Maestroe conectados actualmente</h4>
                                    </div>
                                </div>
                                <div class="col-xs-12 noP">
                                    <div class="media">
                                        <div class="media-left media-middle">
                                            <a href="#">
                                                <img class="media-object img-circle" src="../img/colegios.png">
                                            </a>
                                        </div>
                                        <div class="media-body media-middle">
                                            <h5 class="media-heading">Nombre del colegio</h5>
                                        </div>
                                    </div>

                                    <div class="media">
                                        <div class="media-left media-middle">
                                            <a href="#">
                                                <img class="media-object img-circle" src="../img/colegios.png">
                                            </a>
                                        </div>
                                        <div class="media-body media-middle">
                                            <h5 class="media-heading">Nombre del colegio</h5>
                                        </div>
                                    </div>

                                    <div class="media">
                                        <div class="media-left media-middle">
                                            <a href="#">
                                                <img class="media-object img-circle" src="../img/colegios.png">
                                            </a>
                                        </div>
                                        <div class="media-body media-middle">
                                            <h5 class="media-heading">Nombre del colegio</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--container-->
        </div>
        <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->


    <script>
        $(document).ready(function () {
            // Seccion active (MENU)
            $(document).ready(function () {
                $('#estadisticas').addClass('activo');
                $('#estadisticasli').addClass('activoli');
            });

            // Grafico 1 estilo PIE
            Highcharts.chart('grafico1', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: null
                },
                tooltip: {
                    pointFormat: '<b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }
                },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data: [{
                        name: 'Niños',
                        y: 56.33
                    }, {
                        name: 'Representantes',
                        y: 24.03,
                        sliced: true,
                        selected: true
                    }, {
                        name: 'Maestros',
                        y: 10.38
                    }]
                }]
            });

            // Grafico 2 estilo LINEAL
            Highcharts.chart('grafico2', {
                chart: {
                    type: 'line'
                },
                title: {
                    text: 'Rendimiento mensual'
                },
                xAxis: {
                    categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
                },
                yAxis: {
                    title: {
                        text: null
                    }
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
                series: [{
                    name: '2017',
                    data: [7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
                }, {
                    name: '2016',
                    data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
                }]
            });

            // Grafico 3 estilo COLUMNAS
            Highcharts.chart('grafico3', {
                chart: {
                    type: 'bar'
                },
                title: {
                    text: 'Stacked bar chart'
                },
                xAxis: {
                    categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Total fruit consumption'
                    }
                },
                legend: {
                    reversed: true
                },
                plotOptions: {
                    series: {
                        stacking: 'normal'
                    }
                },
                series: [{
                    name: 'John',
                    data: [5, 3, 4, 7, 2]
                }, {
                    name: 'Jane',
                    data: [2, 2, 3, 2, 1]
                }, {
                    name: 'Joe',
                    data: [3, 4, 4, 2, 5]
                }]
            });

            Highcharts.chart('grafico4', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: 0,
                    plotShadow: false
                },
                title: {
                    text: 'Browser<br>shares<br>2015',
                    align: 'center',
                    verticalAlign: 'middle',
                    y: 40
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: true,
                            distance: -50,
                            style: {
                                fontWeight: 'bold',
                                color: 'white'
                            }
                        },
                        startAngle: -90,
                        endAngle: 90,
                        center: ['50%', '75%']
                    }
                },
                series: [{
                    type: 'pie',
                    name: 'Browser share',
                    innerSize: '50%',
                    data: [
                        ['Firefox',   10.38],
                        ['IE',       56.33],
                        ['Chrome', 24.03],
                        ['Safari',    4.77],
                        ['Opera',     0.91],
                        {
                            name: 'Proprietary or Undetectable',
                            y: 0.2,
                            dataLabels: {
                                enabled: false
                            }
                        }
                    ]
                }]
            });
        });
    </script>

<?php footer(); ?>
