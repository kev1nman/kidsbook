<?php require_once("master.php"); cabecera(); ?>
    <div id='wrapper' class='toggled'>
        <?php sideBar(); ?>

        <!-- Page Content -->
        <div id='page-content-wrapper'>
            <?php topBar(); ?>
                <div class='container'>
                    <div class="col-md-12 noP">
                        <form>
                            <div class="col-md-12 text-center vistaLogin">
                                <div class="col-md-12 noP">
                                    <div class="col-md-12">
                                        <h2>Información del nivel <img class="imgTtitulo" src="../img/niveles.png" alt=""></h2> <br>
                                    </div>
                                </div>

                                <div class="col-md-12 botonesNiveles2">
                                    <strong class="total bg-success">Cupos disponibles en aula</strong>
                                    <strong class="total bg-warning">Aula casi llena</strong>
                                    <strong class="total bg-danger">Aula llena</strong>
                                </div>

                                <div class="col-md-12 totales">
                                    <div class="col-md-6">
                                        <h1>Nivel ositos</h1>
                                        <strong class="">Unica</strong>
                                        <strong class="">A1</strong>
                                    </div>
                                    <div class="col-md-6">
                                        <h1>Estado del aula</h1>
                                        Capacidad de niños en aula actualmente
                                        <strong class="bg-danger">20 niños</strong><br><br>
                                        Capacidad de niños permitidos en el aula
                                        <strong class="bg-danger">20 niños</strong>
                                    </div>
                                    <div class="col-md-6">
                                        <img src="../img/Maestros2.png" alt="">
                                        <strong class="">2 Maestros asignados</strong>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="total total2">
                                            <ul>
                                                <li>Maestro numero 1</li>
                                                <li>Maestro numero 2</li>
                                            </ul>
                                        </div>
                                    </div>

                                </div>




                                <div class="col-md-12">
                                    <a class="btn btn-warning" href="dirNiveles.php">Volver</a>
                                </div>


                            </div>

                        </form>
                    </div>
                </div><!--container-->
        </div>
        <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->


    <script>
        $(document).ready(function () {
            // Seccion active (MENU)
            $(document).ready(function () {
                $('#niveles').addClass('activo');
                $('#nivelesli').addClass('activoli');
            });

        });
    </script>

<?php footer(); ?>
