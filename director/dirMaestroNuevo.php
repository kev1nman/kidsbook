<?php require_once("master.php"); cabecera(); ?>
    <div id='wrapper' class='toggled'>
        <?php sideBar(); ?>

        <!-- Page Content -->
        <div id='page-content-wrapper'>
            <?php topBar(); ?>
                <div class='container'>
                    <div class="col-md-12 noP">
                        <form>
                            <div class="col-md-12 text-center vistaLogin">
                                <div class="col-md-12 noP">
                                    <div class="col-md-12">
                                        <h2>Agregar maestro <img class="imgTtitulo" src="../img/Maestros2.png" alt=""></h2> <br>
                                    </div>
                                </div>
                                <p class="text-danger">(*) Campos obligatorios</p>

                                <div class="col-md-8 col-md-offset-2 noP">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Nombre del Maestro (*)">
                                        </div>

                                        <div class="form-group">
                                            <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Teléfono del Maestro (*)">
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Apellido del Maestro (*)">
                                        </div>

                                        <div class="form-group">
                                            <input type="email" class="form-control loginInput" id="exampleInputEmail1" placeholder="Correo Eléctronico del Maestro (*)">
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-md-offset-3">
                                        <div class="form-group">
                                            <select class="form-control loginInput" name="">
                                                 <option value="">Nivel (*)</option>
                                                 <option value="">1</option>
                                                 <option value="">2</option>
                                                 <option value="">3</option>
                                            </select>
                                        </div>
                                    </div>


                                <div class="col-md-12">
                                    <a class="btn btn-success" href="#">Guardar</a>
                                    <a class="btn btn-warning" href="dirMaestros.php">Volver</a>
                                </div>
                            </div>

                        </form>
                    </div>
                </div><!--container-->
        </div>
        <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->


    <script>
        $(document).ready(function () {
            // Seccion active (MENU)
            $(document).ready(function () {
                $('#Maestro').addClass('activo');
                $('#Maestroli').addClass('activoli');
            });

        });
    </script>

<?php footer(); ?>
