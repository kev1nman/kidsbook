<?php require_once("master.php"); cabecera(); ?>
    <div id='wrapper' class='toggled'>
        <?php sideBar(); ?>

        <!-- Page Content -->
        <div id='page-content-wrapper'>
            <?php topBar(); ?>
                <div class='container'>
                    <div class="col-md-12">
                        <form>
                            <div class="col-md-12 text-center vistaLogin noP">
                                <div class="col-md-12 noP">
                                    <div class="col-md-12">
                                        <h2>Agregar categoria <img class="imgTtitulo" src="img/categoria.png" alt=""></h2> <br>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-md-6 col-md-offset-3 noP">
                                    <div class="agregarCategoria">
                                        <div class="fondoCategoria">
                                            <h4 class="colorCategoria">Color <span class="glyphicon glyphicon-pencil"></span></h4>
                                            <div class="boxImgCategoria">
                                                <span class="glyphicon glyphicon-paperclip imgCategoria"></span> <br>
                                                <h4>Imagen</h4>
                                            </div>
                                            <h2 class="">Nombre de la categoria <span class="glyphicon glyphicon-pencil"></span></h2>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-xs-12">
                                    <br><br>
                                    <a class="btn btn-success" href="#">Guardar</a>
                                    <a class="btn btn-warning" href="admCategorias.php">Volver</a>
                                </div>


                            </div>

                        </form>
                    </div>
                </div><!--container-->
        </div>
        <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->


    <script>
        $(document).ready(function () {
            // Seccion active (MENU)
            $(document).ready(function () {
                $('#publicid').addClass('activo');
                $('#publicidli').addClass('activoli');
            });

        });
    </script>

<?php footer(); ?>
