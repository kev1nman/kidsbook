<?php require_once("master.php"); cabecera(); ?>
    <div id='wrapper' class='toggled'>
        <?php sideBar(); ?>

        <!-- Page Content -->
        <div id='page-content-wrapper'>
            <?php topBar(); ?>
                <div class='container-fluid'>
                    <div class='row'>
                        <div class='col-md-12 noP fila'>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-7 noP">
                                    <h2 class='titulo2'>Notificación</h2>
                                </div>
                                <div class="col-xs-5 noP">
                                    <img class="imgTtitulo" src="../img/notificacion.png" alt="">
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-12 recuadroInfo sombra noP">
                                    <img class="imgRecuadro" src="../img/ninos.png" alt="">
                                    <h3 class="titulo3">
                                        Niños <br>
                                        <small>542</small>
                                    </h3>
                                    <img class="imgRecuadro2" src="../img/mundo.png" alt="">
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-12 recuadroInfo sombra noP">
                                    <img class="imgRecuadro" src="../img/representantes.png" alt="">
                                    <h3 class="titulo3">
                                        Representantes <br>
                                        <small>854</small>
                                    </h3>
                                    <img class="imgRecuadro2" src="../img/mundo.png" alt="">
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-12 recuadroInfo sombra noP">
                                    <img class="imgRecuadro" src="../img/niveles2.png" alt="">
                                    <h3 class="titulo3">
                                        Nivel Asignado: <br>
                                        <small>Ositos</small>
                                    </h3>
                                    <img class="imgRecuadro2" src="../img/mundo.png" alt="">
                                </div>
                            </div>

                        </div>
                        <div class="col-xs-12 noP">
                            <div class="panel panel-default">
                                <!-- Default panel contents -->
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-md-12 addNewBtn">
                                            <!-- Button trigger modal -->
                                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#menu">
                                                Enviar notificación
                                                <span class="glyphicon glyphicon-send"></span>
                                            </button>

                                            <!-- Button trigger modal -->
                                            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#menu2">
                                                Lista de receptores
                                                <span class="glyphicon glyphicon-send"></span>
                                            </button>

                                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#menu3">
                                                Enviar
                                                <span class="glyphicon glyphicon-camera"></span>
                                                <span class="glyphicon glyphicon-film"></span>
                                            </button>

                                        <!-- Modal- Nueva notif -->
                                        <div class="modal fade" id="menu" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                          <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h2 class="modal-title text-center titulo4" id="myModalLabel">Crear nueva notificación <span class="glyphicon glyphicon-send"></span></h2>
                                              </div>
                                              <div class="modal-body">
                                                  <div class="col-md-12 noP">
                                                  <div class="col-md-4">
                                                          <select class="form-control select" name="tipoNot" onChange="Mostrar(this)">
                                                              <option value="1">Seleccione el tipo</option>
                                                              <OPTION VALUE="Informativa">Informativa</OPTION>
                                                              <OPTION VALUE="Asistencia">Asistencia</OPTION>
                                                          </select>
                                                  </div>

                                                   <div class='col-md-4' id='calendario' style="display:none;">
                                                         <div class="form-group">
                                                          <div class='input-group date' id='datetimepicker1'>
                                                          <input type='text' placeholder="Fecha y Hora"  class="sinB" />
                                                              <span class="input-group-addon">
                                                                 <span class="glyphicon glyphicon-calendar"></span>
                                                             </span>
                                                          </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                          <div class="form-group">
                                                              <input type="checkbox" name="" value="" id="enviarTodos" >
                                                              <label for="enviarTodos"> Enviar a todos</label>
                                                          </div>
                                                        </div> 
                                                      </div>
                                                    <div class="col-md-4">
                            
                                                           <select class="form-control select" name="tipoNot" onChange="Mostrar(this)">
                                                              <option value="1">Lista de afiliados</option>

                                                          </select>
                                                      
                                                  </div>
                                                  <div class="col-md-12 usuariosON">
                                                      <input class="form-control form-control2" type="text" name="" value="" placeholder="Título de la notificación">
                                                      <textarea class="form-control form-control2" name="name" rows="8" cols="80" placeholder="Mensaje"></textarea>
                                                  </div>

                                              </div>
                                              <div class="modal-footer">
                                                <button class="btn btn-success" type="button" name="button">Enviar</button>
                                              </div>
                                            </div>
                                          </div>
                                        </div>



                                        <!-- Modal- Nueva notif -->
                                        <div class="modal fade" id="menu2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                          <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h2 class="modal-title text-center titulo4" id="myModalLabel">Lista de receptores<span class="glyphicon glyphicon-send"></span></h2>
                                              </div>
                                              <div class="modal-body">
                                                  <div class="col-md-12 noP">
                                                      <div class="col-md-6">
                                                          <select class="form-control select" name="">
                                                              <option value="1">Tipo de notificación</option>
                                                          </select>
                                                      </div>
                                                      <div class="col-md-6">
                                                          <select class="form-control select" name="">
                                                              <option value="1">Lista de afiliados</option>
                                                          </select>
                                                      </div>
                                                       
                
                                                      <div class="col-md-12">     <br>   </div>
                                                  </div>

                                                  <div class="col-md-12 table-responsive usuariosON2">

                                                      <table class="table">
                                                          <tr>
                                                              <td class="checking"><input type="checkbox" name="" value=""></td>
                                                              <td> Angela Vitale</td>
                                                              <td>04148907654</td>
                                                              <td>correo@mail.com</td>
                                                              <td><span class="glyphicon glyphicon-ok text-success"></span><span class="glyphicon glyphicon-ok text-success"></span></td>
                                                              <td class="checking"><input type="radio" name="" value="" chekked></td>
                                                              <td>Asistirá</td>
                                                              <td>Leído 1 de 1</td>
                                                              <td>Asistiran 8 de 10</td>
                                                          </tr>
                                                          <tr>
                                                              <td class="checking"><input type="checkbox" name="" value=""></td>
                                                              <td> Angela Vitale</td>
                                                              <td>04148907654</td>
                                                              <td>correo@mail.com</td>
                                                              <td><span class="glyphicon glyphicon-ok text-success"></span><span class="glyphicon glyphicon-ok text-success"></span></td>
                                                              <td class="checking"><input type="radio" name="" value="" chekked></td>
                                                              <td>Asistirá</td>
                                                              <td>Leído 1 de 1</td>
                                                              <td>Asistiran 10 de 10</td>
                                                          </tr>
                                                          <tr>
                                                              <td class="checking"><input type="checkbox" name="" value=""></td>
                                                              <td> Angela Vitale</td>
                                                              <td>04148907654</td>
                                                              <td>correo@mail.com</td>
                                                              <td><span class="glyphicon glyphicon-ok text-success"></span><span class="glyphicon glyphicon-ok text-success"></span></td>
                                                              <td class="checking"><input type="radio" name="" value="" chekked></td>
                                                              <td>Asistirá</td>
                                                              <td>Leído 1 de 1</td>
                                                              <td>Asistiran 8 de 10</td>
                                                          </tr>
                                                      </table>
                                                  </div>

                                              </div>
                                              <div class="modal-footer">
                                                <button class="btn btn-warning" type="button" name="button" data-dismiss="modal" aria-label="close">Cerrar</button>
                                              </div>
                                            </div>
                                          </div>
                                        </div>

                                        <!-- Modal- Nueva notif -->
                                            <div class="modal fade" id="menu3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                              <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h2 class="modal-title text-center titulo4" id="myModalLabel">Subir una foto o video <span class="glyphicon glyphicon-camera"></span>
                                                    <span class="glyphicon glyphicon-film"></span></h2>
                                                  </div>
                                                  <div class="modal-body">

                                                      <p>Comparte directamente las fotos y videos con los representantes asignados</p>

                                                      <div class="col-md-12 text-center noP">

                                                          <div class="form-group">
                                                              <select class="form-control select" name="">
                                                                  <option value="1">Nombre del niño</option>
                                                              </select>
                                                          </div>

                                                          <div class="form-group">
                                                              <select class="form-control select" name="">
                                                                  <option value="1">Representantes asignados</option>
                                                              </select>
                                                          </div>


                                                          <div class="form-group borderCuadro subirBox">
                                                              <label for="ejemplo_archivo_1">
                                                                  <span class="icon-upload3 subirIncono"></span>
                                                              </label>
                                                              <input type="file" id="ejemplo_archivo_1">
                                                              <h2 class="help-block">Subir foto o video jpg/png/gif/mp4/mpeg</h2>
                                                          </div>



                                                      </div>

                                                  </div>
                                                  <div class="modal-footer">
                                                    <button class="btn btn-success" type="button" name="button" data-dismiss="modal" aria-label="close">Compartir</button>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>

                                    </div>
                                </div>
                                <div class="panel-body">
                                    <button type="button" class="btn btn-default disabled" name="button"><span class="glyphicon glyphicon-trash"></span> Borrar seleccionados</button>
                                </div>

                                <div class="table-responsive">
                                    <!-- Table -->
                                    <table id="notificaciones" class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th>Remitente</th>
                                                <th>Fecha</th>
                                                <th>Tipo</th>
                                                <th>Título</th>
                                                <th>Acciones</th>
                                            </tr>

                                        </thead>
                                        <tr>
                                            <td class="checking"><input type="checkbox" name="" value=""></td>
                                            <td class="checking"><span class="icon-star-full text-warning starColor"></span></td>
                                            <td> KidsBook</td>
                                            <td>21/02/2017</td>
                                            <td>Informativo</td>
                                            <td>Aviso importante</td>
                                            <td>

                                                <span class="glyphicon glyphicon-trash iconTable"></span>
                                                <span class="glyphicon glyphicon-eye-open iconTable"></span>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="checking"><input type="checkbox" name="" value=""></td>
                                            <td class="checking"><span class="icon-star-full text-warning starColor"></span></td>
                                            <td> KidsBook</td>
                                            <td>21/02/2017</td>
                                            <td>Informativo</td>
                                            <td>Aviso importante</td>
                                            <td>

                                                <span class="glyphicon glyphicon-trash iconTable"></span>
                                                <span class="glyphicon glyphicon-eye-open iconTable"></span>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="checking"><input type="checkbox" name="" value=""></td>
                                            <td class="checking"><span class="icon-star-full text-warning starColor"></span></td>
                                            <td> KidsBook</td>
                                            <td>21/02/2017</td>
                                            <td>Informativo</td>
                                            <td>Aviso importante</td>
                                            <td>

                                                <span class="glyphicon glyphicon-trash iconTable"></span>
                                                <span class="glyphicon glyphicon-eye-open iconTable"></span>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="checking"><input type="checkbox" name="" value=""></td>
                                            <td class="checking"><span class="icon-star-full text-warning starColor"></span></td>
                                            <td> KidsBook</td>
                                            <td>21/02/2017</td>
                                            <td>Informativo</td>
                                            <td>Aviso importante</td>
                                            <td>

                                                <span class="glyphicon glyphicon-trash iconTable"></span>
                                                <span class="glyphicon glyphicon-eye-open iconTable"></span>

                                            </td>
                                        </tr>

                                    </table>
                                </div>
                                <div class="panel-body">
                                    <button type="button" class="btn btn-default disabled" name="button"><span class="glyphicon glyphicon-trash"></span> Borrar seleccionados</button>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-8 totales">
                            <div class="col-md-6">
                                Total notificaciones enviadas
                                <strong class="bg-success">110</strong>
                            </div>
                            <div class="col-md-6">
                                Total notificaciones enviadas
                                <strong class="bg-warning">55</strong>
                            </div>
                            <div class="col-md-6">
                                Total notificaciones enviadas
                                <strong class="bg-info">110</strong>
                            </div>
                            <div class="col-md-6">
                                Total notificaciones enviadas
                                <strong class="bg-danger">55</strong>
                            </div>

                        </div>


                    </div>
                </div><!--container-->
        </div>
        <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->


    <script>
        $(document).ready(function () {
            // Seccion active (MENU)
            $(document).ready(function () {
                $('#notificacion').addClass('activo');
                $('#notificacionli').addClass('activoli');
            });

            $(document).ready(function() {
                $('#notificaciones').DataTable();
            } );

        });
    </script>

    <script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker();
            });
    </script>

    <script>

    function Mostrar(sel) {
      if (sel.value=="Asistencia"){
           divC = document.getElementById("calendario");
           divC.style.display = "";
        }else{
          divC.style.display = "none";
        }
      }
    </script>


<?php footer(); ?>
