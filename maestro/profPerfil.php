<?php require_once("master.php"); cabecera(); ?>
    <div id='wrapper' class='toggled'>
        <?php sideBar(); ?>

        <!-- Page Content -->
        <div id='page-content-wrapper'>
            <?php topBar(); ?>
                <div class='container'>
                    <div class="row">
                        <div class="col-md-12 noP">
                            <form>
                                <div class="col-md-12 text-center vistaLogin">
                                    <div class="col-md-12 noP">
                                        <div class="col-md-12">
                                            <h2>Mi perfil <img src="../img/koala.png" alt=""></h2> <br>
                                        </div>
                                    </div>

                                   <div class="col-md-8 col-md-offset-2 noP">

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Nombre del Maestro (*)">
                                            </div>

                                            <div class="form-group">
                                                <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Teléfono del Maestro (*)">
                                            </div>

                                            <div class="form-group">
                                                <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Nivel asignado (*)">
                                            </div>

                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Apellido del Maestro (*)">
                                            </div>

                                            <div class="form-group">
                                                <input type="email" class="form-control loginInput" id="exampleInputEmail1" placeholder="Correo Eléctronico del Maestro (*)">
                                            </div>

                                        </div>

                                        <div class="col-md-6">
                                            <div class="col-md-12">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Sección (*)">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Aula (*)">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                         
                                          
                                            <!-- Modal- Nueva notif -->
                                            <div class="modal fade" id="menu" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                              <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h2 class="modal-title text-center titulo4" id="myModalLabel">Subir una foto o video <span class="glyphicon glyphicon-camera"></span>
                                                    <span class="glyphicon glyphicon-film"></span></h2>
                                                  </div>
                                                  <div class="modal-body">

                                                      <p>Comparte directamente las fotos y videos con los representantes asignados</p>

                                                      <div class="col-md-12">

                                                          <div class="form-group">
                                                              <select class="form-control select" name="">
                                                                  <option value="1">Nombre del niño</option>
                                                              </select>
                                                          </div>

                                                          <div class="form-group">
                                                              <select class="form-control select" name="">
                                                                  <option value="1">Representantes asignados</option>
                                                              </select>
                                                          </div>


                                                          <div class="form-group borderCuadro subirBox">
                                                              <label for="ejemplo_archivo_1">
                                                                  <span class="icon-upload3 subirIncono"></span>
                                                              </label>
                                                              <input type="file" id="ejemplo_archivo_1">
                                                              <h2 class="help-block">Subir foto o video jpg/png/gif/mp4/mpeg</h2>
                                                          </div>



                                                      </div>

                                                  </div>
                                                  <div class="modal-footer">
                                                    <button class="btn btn-success" type="button" name="button" data-dismiss="modal" aria-label="close">Compartir</button>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>





                                            <!-- Modal- Nueva notif -->
                                            <div class="modal fade" id="menu2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                              <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h2 class="modal-title text-center titulo4" id="myModalLabel">Cargar boletín <span class="icon-file-text2"></span></h2>
                                                  </div>
                                                  <div class="modal-body">

                                                      <p>1) Selecciona el nombre del niño</p>
                                                      <p>2) Busca en tu computador el boletín a enviar</p>
                                                      <p>3) Cárgalo y envíalo a los representantes asignados</p>

                                                      <div class="col-md-12">

                                                          <div class="form-group">
                                                              <select class="form-control select" name="">
                                                                  <option value="1">Nombre del niño</option>
                                                              </select>
                                                          </div>

                                                          <div class="form-group">
                                                              <select class="form-control select" name="">
                                                                  <option value="1">Representantes asignados</option>
                                                              </select>
                                                          </div>


                                                          <div class="form-group borderCuadro subirBox">
                                                              <label for="ejemplo_archivo_1">
                                                                  <span class="icon-file-text2 subirIncono"></span>
                                                              </label>
                                                              <input type="file" id="ejemplo_archivo_1">
                                                              <h2 class="help-block">Cargar documento doc/pdf/jpg/png</h2>
                                                          </div>



                                                      </div>

                                                  </div>
                                                  <div class="modal-footer">
                                                    <button class="btn btn-success" type="button" name="button" data-dismiss="modal" aria-label="close">Compartir</button>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>

                                        </div>


                                        <div class="col-md-12 fila">
                                            <a class="btn btn-warning" href="profEstadisticas.php">Volver</a>
                                        </div>

                                    </div>

                                </div>
                            </form>
                        </div>

                    </div>
                </div><!--container-->
        </div>
        <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->


    <script>
        $(document).ready(function () {
            // Seccion active (MENU)
            $(document).ready(function () {
                $('#perfil').addClass('activo');
                $('#perfilli').addClass('activoli');
            });

        });
    </script>

<?php footer(); ?>
