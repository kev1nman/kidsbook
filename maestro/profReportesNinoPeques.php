<?php require_once("master.php"); cabecera(); ?>
    <div id='wrapper' class='toggled'>
        <?php sideBar(); ?>

        <!-- Page Content -->
        <div id='page-content-wrapper'>
            <?php topBar(); ?>
                <div class='container-fluid'>
                    <div class='row'>
                        <div class='col-md-12 noP fila'>
                            <div class="col-md-6 col-lg-4 recuadro">
                                <div class="col-xs-8 noP">
                                    <h2 class='titulo2'>Agregar reporte</h2>
                                </div>
                                <div class="col-xs-4 noP">
                                    <img class="imgTtitulo" src="../img/reportes.png" alt="">
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-4 recuadro">
                                <div class="col-xs-12 noP">
                                    <form>
                                     <div class="form-group">
                                         <br>
                                         <select class="form-control">
                                             <option>Nombre del Niño</option>
                                             <option>Jhan</option>
                                             <option>Mario</option>
                                             <option>Juana</option>
                                        </select>
                                    </div>
                                   </form>
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-4 recuadro text-center ">
                                <div class=" col-xs-12 ">
                                    <h3 class="titulo3">
                                        Estado del Reporte
                                        <p><span class="icon-cross text-danger"></span> no enviado</p>

                                      </h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 noP">
                            <div class='col-md-4 col-xs-12 recuadrop  '>
                                <div class="col-xs-12 ficha">
                                    <img class="imgRecuadro img-circle" src="../img/ninos.png" alt="">
                                    <h4>Ficha Personal del Niño</h4>
                                    <p>Nombre y Apellido</p>
                                    <p>Edad</p>
                                    <img class="img2" src="../img/nina.png" alt="">
                                </div>
                            </div>

                            <div class='col-md-4 col-xs-12 recuadrop  '>
                                <div class="col-xs-12 ficha">
                                    <img class="imgRecuadro img-circle" src="../img/ninos.png" alt="">
                                    <h4>Representante1</h4>
                                    <p>Nombre y Apellido</p>
                                    <p>Número de teléfono</p>
                                </div>
                            </div>

                            <div class='col-md-4 col-xs-12 recuadrop  '>
                                <div class="col-xs-12 ficha">
                                    <img class="imgRecuadro img-circle" src="../img/ninos.png" alt="">
                                    <h4>Representante2</h4>
                                    <p>Nombre y Apellido</p>
                                    <p>Número de teléfono</p>
                                </div>
                            </div>
                        </div>


                        <div class="col-xs-12 col-md-6">
                            <div class="col-xs-12 noP cuadroReportes">
                                <div class="col-md-2">
                                      <h4>Desayuno</h4>
                                </div>
                                <div class="col-xs-2 text-center">
                                      <img class="midOpacity imgReportes" src="../img/tetero.png" alt="">
                                      <h5>No comió</h5>
                                </div>
                                <div class="col-xs-2 text-center">
                                      <img class="midOpacity imgReportes" src="../img/tetero.png" alt="">
                                      <h5>1/4</h5>
                                </div>
                                <div class="col-xs-2 text-center">
                                      <img class="imgReportes" src="../img/tetero.png" alt="">
                                      <h5>1/2</h5>
                                </div>
                                <div class="col-xs-2 text-center">
                                      <img class="midOpacity imgReportes" src="../img/tetero.png" alt="">
                                      <h5>3/4</h5>
                                </div>
                                <div class="col-xs-2 text-center">
                                      <img class="midOpacity imgReportes" src="../img/tetero.png" alt="">
                                      <h5>Todo</h5>
                                </div>
                            </div>

                            <div class='col-xs-12 col-md-6 noP'>
                                <div class='form-group'>
                                    <label for="tiempo" class="pointer labelPremios">
                                       <img class="imgReportes2" src="../img/tetero.png" alt="">
                                        <span>¿ Cuántos ? </span>
                                    </label>
                                    <input id="tiempo" class="inputPremios" type="text" name="tiempo">
                                </div>
                            </div>

                             <div class='col-xs-12 noP'>
                                 <div class='form-group'>
                                     <textarea class="form-control form-control3" rows="3" placeholder="Asignaciones Importantes"></textarea>
                                 </div>
                             </div>

                             <div class='col-xs-12 noP'>
                                <div class='form-group'>
                                    <textarea class="form-control form-control3" rows="3" placeholder="Mensaje"></textarea>
                                </div>
                             </div>
                        </div>

                        <div class="col-xs-12 col-md-6">
                            <form>

                                <div class='col-xs-12 col-md-6 noP'>
                                    <div class='form-group'>
                                        <label for="durmio" class="pointer">
                                           <img class="imgReportes2" src="../img/durmio.png" alt="">
                                            <span>Durmió</span>
                                        </label>
                                        <input id="durmio" class="checking" type="checkbox" name="durmio">
                                    </div>
                                </div>

                                <div class='col-xs-12 col-md-6 noP'>
                                    <div class='form-group'>
                                        <label for="tiempo" class="pointer labelPremios">
                                           <img class="imgReportes2" src="../img/reloj.png" alt="">
                                            <span>Cuanto tiempo durmió</span>
                                        </label>
                                        <input id="tiempo" class="inputPremios" type="text" name="tiempo">
                                    </div>
                                </div>

                                <div class='col-xs-12 col-md-6 noP'>
                                    <div class='form-group'>
                                        <label for="bano" class="pointer labelPremios">
                                           <img class="imgReportes2" src="../img/bano.png" alt="">
                                            <span>Cambio de pañales</span>
                                        </label>
                                        <input id="bano" class="inputPremios" type="text" name="bano">
                                    </div>
                                </div>

                                <div class='col-xs-12 col-md-6 noP'>
                                    <div class='form-group'>
                                        <label for="liquido" class="pointer labelPremios">
                                           <img class="imgReportes2" src="../img/bano2.png" alt="">
                                            <span>Líquido, Nº de veces</span>
                                        </label>
                                        <input id="liquido" class="inputPremios" type="text" name="liquido">
                                    </div>
                                </div>

                                <div class='col-xs-12 col-md-6 noP'>
                                    <div class='form-group'>
                                        <label for="solido" class="pointer labelPremios">
                                           <img class="imgReportes2" src="../img/bano2.png" alt="">
                                            <span>Solido, Nº de veces</span>
                                        </label>
                                        <input id="solido" class="inputPremios" type="text" name="solido">
                                    </div>
                                </div>



                            </form>

                        </div>


                        <!-- Modal- Nueva notif -->
                        <div class="modal fade" id="menu" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                          <div class="modal-dialog premios" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h2 class="modal-title text-center titulo4" id="myModalLabel">Agregar premios</h2>
                              </div>
                              <div class="modal-body">
                                  <div class="col-md-12 noP">

                                    <div class="col-md-12">
                                        <p>Ahora puedes agregar hasta 7 premios aleatorios o 7 unidades de un mismo premio.</p>
                                        <p>Introduce una cantidad del 1 al 7</p>
                                    </div>
                                  </div>

                                    <div class="col-md-12 usuariosON">
                                        <form class="form-inline">
                                            <table class="table">
                                                <tr>
                                                    <td>
                                                        <div class="col-xs-4 noP">
                                                            <img src="../img/conejo.png" class="imgPremios" alt="">
                                                        </div>
                                                        <div class="col-xs-8 form-group form-control-inline noP">
                                                            <label for="conejo" class="labelPremios">Cantidad: </label>
                                                            <input type="text" class="form-control2 inputPremios" id="conejo" placeholder="">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <div class="col-xs-4 noP">
                                                            <img src="../img/carro2.png" class="imgPremios" alt="">
                                                        </div>
                                                        <div class="col-xs-8 form-group form-control-inline noP">
                                                            <label for="carro2" class="labelPremios">Cantidad: </label>
                                                            <input type="text" class="form-control2 inputPremios" id="carro2" placeholder="">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <div class="col-xs-4 noP">
                                                            <img src="../img/avion.png" class="imgPremios" alt="">
                                                        </div>
                                                        <div class="col-xs-8 form-group form-control-inline noP">
                                                            <label for="avion" class="labelPremios">Cantidad: </label>
                                                            <input type="text" class="form-control2 inputPremios" id="avion" placeholder="">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <div class="col-xs-4 noP">
                                                            <img src="../img/oso2.png" class="imgPremios" alt="">
                                                        </div>
                                                        <div class="col-xs-8 form-group form-control-inline noP">
                                                            <label for="oso2" class="labelPremios">Cantidad: </label>
                                                            <input type="text" class="form-control2 inputPremios" id="oso2" placeholder="">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <div class="col-xs-4 noP">
                                                            <img src="../img/tren.png" class="imgPremios" alt="">
                                                        </div>
                                                        <div class="col-xs-8 form-group form-control-inline noP">
                                                            <label for="tren" class="labelPremios">Cantidad: </label>
                                                            <input type="text" class="form-control2 inputPremios" id="tren" placeholder="">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <div class="col-xs-4 noP">
                                                            <img src="../img/carro.png" class="imgPremios" alt="">
                                                        </div>
                                                        <div class="col-xs-8 form-group form-control-inline noP">
                                                            <label for="carro" class="labelPremios">Cantidad: </label>
                                                            <input type="text" class="form-control2 inputPremios" id="carro" placeholder="">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <div class="col-xs-4 noP">
                                                            <img src="../img/cometa.png" class="imgPremios" alt="">
                                                        </div>
                                                        <div class="col-xs-8 form-group form-control-inline noP">
                                                            <label for="cometa" class="labelPremios">Cantidad: </label>
                                                            <input type="text" class="form-control2 inputPremios" id="cometa" placeholder="">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <div class="col-xs-4 noP">
                                                            <img src="../img/caramelo.png" class="imgPremios" alt="">
                                                        </div>
                                                        <div class="col-xs-8 form-group form-control-inline noP">
                                                            <label for="caramelo" class="labelPremios">Cantidad: </label>
                                                            <input type="text" class="form-control2 inputPremios" id="caramelo" placeholder="">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <div class="col-xs-4 noP">
                                                            <img src="../img/chupeta.png" class="imgPremios" alt="">
                                                        </div>
                                                        <div class="col-xs-8 form-group form-control-inline noP">
                                                            <label for="chupeta" class="labelPremios">Cantidad: </label>
                                                            <input type="text" class="form-control2 inputPremios" id="chupeta" placeholder="">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <div class="col-xs-4 noP">
                                                            <img src="../img/oro.png" class="imgPremios" alt="">
                                                        </div>
                                                        <div class="col-xs-8 form-group form-control-inline noP">
                                                            <label for="oro" class="labelPremios">Cantidad: </label>
                                                            <input type="text" class="form-control2 inputPremios" id="oro" placeholder="">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <div class="col-xs-4 noP">
                                                            <img src="../img/plata.png" class="imgPremios" alt="">
                                                        </div>
                                                        <div class="col-xs-8 form-group form-control-inline noP">
                                                            <label for="plata" class="labelPremios">Cantidad: </label>
                                                            <input type="text" class="form-control2 inputPremios" id="plata" placeholder="">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <div class="col-xs-4 noP">
                                                            <img src="../img/bronce.png" class="imgPremios" alt="">
                                                        </div>
                                                        <div class="col-xs-8 form-group form-control-inline noP">
                                                            <label for="bronce" class="labelPremios">Cantidad: </label>
                                                            <input type="text" class="form-control2 inputPremios" id="bronce" placeholder="">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <div class="col-xs-4 noP">
                                                            <img src="../img/star.png" class="imgPremios" alt="">
                                                        </div>
                                                        <div class="col-xs-8 form-group form-control-inline noP">
                                                            <label for="star" class="labelPremios">Cantidad: </label>
                                                            <input type="text" class="form-control2 inputPremios" id="star" placeholder="">
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </form>
                                    </div>

                              </div>
                              <div class="modal-footer">
                                  <button class="btn btn-success" type="button" name="button" data-dismiss="modal" aria-label="close">Agregar al reporte</button>
                                  <button class="btn btn-warning" type="button" name="button" data-dismiss="modal" aria-label="close">Cerrar</button>
                              </div>
                            </div>
                          </div>
                        </div><!-- Modal- Nueva notif -->




                        <!-- Modal- Nueva notif -->
                        <div class="modal fade" id="menu2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                          <div class="modal-dialog premios" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h2 class="modal-title text-center titulo4" id="myModalLabel"><img src="../img/conejo.png" class="imgPremios imgReportes" alt=""> Estado de ánimo</h2>
                                <h3 class="titulo2">Hoy estuve:</h3>
                              </div>
                              <div class="modal-body">

                                    <div class="col-md-12 usuariosON">
                                        <form class="form-inline">
                                            <table class="table">
                                                <tr>
                                                    <td>
                                                        <div class="col-xs-4 noP">
                                                            <img src="../img/feliz2.png" class="imgPremios" alt="">
                                                        </div>
                                                        <div class="col-xs-8 form-group form-control-inline noP">
                                                            <input type="text" class="formPremio text-center" value="Felíz" placeholder="">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <div class="col-xs-4 noP">
                                                            <img src="../img/sonrisa.png" class="imgPremios" alt="">
                                                        </div>
                                                        <div class="col-xs-8 form-group form-control-inline noP">
                                                            <input type="text" class="formPremio text-center" value="Alegre" placeholder="">                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <div class="col-xs-4 noP">
                                                            <img src="../img/llorar.png" class="imgPremios" alt="">
                                                        </div>
                                                        <div class="col-xs-8 form-group form-control-inline noP">
                                                            <input type="text" class="formPremio text-center" value="Muy Triste" placeholder="">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <div class="col-xs-4 noP">
                                                            <img src="../img/sorprendido.png" class="imgPremios" alt="">
                                                        </div>
                                                        <div class="col-xs-8 form-group form-control-inline noP">
                                                            <input type="text" class="formPremio text-center" value="Inquieto" placeholder="">                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <div class="col-xs-4 noP">
                                                            <img src="../img/molesto.png" class="imgPremios" alt="">
                                                        </div>
                                                        <div class="col-xs-8 form-group form-control-inline noP">
                                                            <input type="text" class="formPremio text-center" value="Irritable" placeholder="">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <div class="col-xs-4 noP">
                                                            <img src="../img/triste.png" class="imgPremios" alt="">
                                                        </div>
                                                        <div class="col-xs-8 form-group form-control-inline noP">
                                                            <input type="text" class="formPremio text-center" value="No me sentí bien" placeholder="">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <div class="col-xs-4 noP">
                                                            <img src="../img/llorar2.png" class="imgPremios" alt="">
                                                        </div>
                                                        <div class="col-xs-8 form-group form-control-inline noP">
                                                            <input type="text" class="formPremio text-center" value="Somnoliento" placeholder="">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <div class="col-xs-4 noP">
                                                            <img src="../img/feliz.png" class="imgPremios" alt="">
                                                        </div>
                                                        <div class="col-xs-8 form-group form-control-inline noP">
                                                            <input type="text" class="formPremio text-center" value="Amistoso" placeholder="">                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <div class="col-xs-4 noP">
                                                            <img src="../img/nerd.png" class="imgPremios" alt="">
                                                        </div>
                                                        <div class="col-xs-8 form-group form-control-inline noP">
                                                            <input type="text" class="formPremio text-center" value="Atento" placeholder="">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <div class="col-xs-4 noP">
                                                            <img src="../img/normal.png" class="imgPremios" alt="">
                                                        </div>
                                                        <div class="col-xs-8 form-group form-control-inline noP">
                                                            <input type="text" class="formPremio text-center" value="Preocupado" placeholder="">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <div class="col-xs-4 noP">
                                                            <img src="../img/picaro.png" class="imgPremios" alt="">
                                                        </div>
                                                        <div class="col-xs-8 form-group form-control-inline noP">
                                                            <input type="text" class="formPremio text-center" value="Amigable" placeholder="">
                                                        </div>
                                                    </td>
                                                </tr>


                                            </table>
                                        </form>
                                    </div>

                              </div>
                              <div class="modal-footer">
                                  <button class="btn btn-success" type="button" name="button" data-dismiss="modal" aria-label="close">Agregar al reporte</button>
                                  <button class="btn btn-warning" type="button" name="button" data-dismiss="modal" aria-label="close">Cerrar</button>
                              </div>
                            </div>
                          </div>
                        </div><!-- Modal- Nueva notif -->





                       <div class='col-xs-12 col-md-10 col-md-offset-1 text-center'>
                            <div class="col-sm-6">
                                 <img class="imgReportes2" src="../img/regalo.png" alt="">
                                 <!-- Button trigger modal -->
                                 <button type="button" class="btn btn-info" data-toggle="modal" data-target="#menu">
                                     Premiar Niño
                                 </button>
                            </div>
                            <div class="col-sm-6">
                                 <img class="imgReportes2" src="../img/oso.png" alt="">
                                 <!-- Button trigger modal -->
                                 <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#menu2">
                                     Indicar estado de ánimo
                                 </button>
                            </div>
                        </div>

                        <div class='col-xs-12 col-md-8 col-md-offset-2 text-center no'>
                             <div class="col-xs-12 col-md-4">
                                 <a class="btn btn-success" href="#">Enviar Reporte</a>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <a class="btn btn-warning" href="#">Guardar Reporte</a>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                 <a class="btn btn-danger" href="profReportes.php">Cancelar</a>
                            </div>
                        </div>
                    </div><!--row-->
                </div><!--container-->
        </div>
        <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->


    <script>
        $(document).ready(function () {
            // Seccion active (MENU)
            $(document).ready(function () {
                $('#reportes').addClass('activo');
                $('#reportesli').addClass('activoli');
            });

            $(document).ready(function() {
                $('#notificaciones').DataTable();
            } );

        });
    </script>

<?php footer(); ?>
