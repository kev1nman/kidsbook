<?php require_once("master.php"); cabecera(); ?>
    <div id='wrapper' class='toggled'>
        <?php sideBar(); ?>

        <!-- Page Content -->
        <div id='page-content-wrapper'>
            <?php topBar(); ?>
                <div class='container-fluid'>
                    <div class='row'>
                        <div class='col-md-12 noP fila'>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-7 noP">
                                    <h2 class='titulo2'>Niños</h2>
                                </div>
                                <div class="col-xs-5 noP">
                                    <img class="imgRecuadro" src="../img/ninos2.png" alt="">
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-12 recuadroInfo sombra noP">
                                    <img class="imgRecuadro" src="../img/ninos.png" alt="">
                                    <h3 class="titulo3">
                                        Niños <br>
                                        <small>542</small>
                                    </h3>
                                    <img class="imgRecuadro2" src="../img/mundo.png" alt="">
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-12 recuadroInfo sombra noP">
                                    <img class="imgRecuadro" src="../img/representantes.png" alt="">
                                    <h3 class="titulo3">
                                        Representantes <br>
                                        <small>854</small>
                                    </h3>
                                    <img class="imgRecuadro2" src="../img/mundo.png" alt="">
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-12 recuadroInfo sombra noP">
                                    <img class="imgRecuadro" src="../img/niveles2.png" alt="">
                                    <h3 class="titulo3">
                                        Niveles <br>
                                        <small>20</small>
                                    </h3>
                                    <img class="imgRecuadro2" src="../img/mundo.png" alt="">
                                </div>
                            </div>

                        </div>

                        <div class="col-xs-12 noP">
                            <div class="panel panel-default">
                                <!-- Default panel contents -->
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-md-12 addNewBtn">
                                            <a class="btn btn-success"  href="profReportesGestion.php">Crear reporte</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <button type="button" class="btn btn-default disabled" name="button"><span class="glyphicon glyphicon-trash"></span> Borrar seleccionados</button>
                                </div>

                                <div class="table-responsive">
                                    <!-- Table -->
                                    <table id="representantes" class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th>Nombre</th>
                                                <th>Edad</th>
                                                <th>Sexo</th>
                                                <th>Representante 1</th>
                                                <th>Representante 2</th>
                                                <th>Acciones</th>
                                            </tr>

                                        </thead>
                                        <tr>
                                            <td class="checking"><input type="checkbox" name="" value=""></td>
                                            <td class="checking"><img class="img-circle imgNino" src="../img/colegios.png"></td>
                                            <td> Jhan Castillo</td>
                                            <td>4 años</td>
                                            <td><img class="imgNino2" src="../img/nino.png"></td></td>
                                            <td>Maria Castillo</td>
                                            <td>Jose Castillo</td>
                                            <td>

                                                <span class="glyphicon glyphicon-trash iconTable"></span>
                                                <a class="text-warning" href="profNinosPerfil.php"><span class="glyphicon glyphicon-eye-open iconTable"></span></a>
                                                <a data-toggle="modal" href="#menu2" class="text-warning" title="Enviar Boletín">
                                                <span class="icon-file-text2"></span></a>
                                                <span class="glyphicon glyphicon-user iconTable"></span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="panel-body">
                                    <button type="button" class="btn btn-default disabled" name="button"><span class="glyphicon glyphicon-trash"></span> Borrar seleccionados</button>
                                </div>
                            </div>
                        </div>

                        <!-- Modal- Nueva notif -->
                                            <div class="modal fade" id="menu2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                              <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h2 class="modal-title text-center titulo4" id="myModalLabel">Cargar boletín <span class="icon-file-text2"></span></h2>
                                                  </div>
                                                  <div class="modal-body">

                                                      <p>1) Selecciona el nombre del niño</p>
                                                      <p>2) Busca en tu computador el boletín a enviar</p>
                                                      <p>3) Cárgalo y envíalo a los representantes asignados</p>

                                                      <div class="col-md-12 text-center noP">

                                                          <div class="form-group">
                                                              <select class="form-control select" name="">
                                                                  <option value="1">Nombre del niño</option>
                                                              </select>
                                                          </div>

                                                          <div class="form-group">
                                                              <select class="form-control select" name="">
                                                                  <option value="1">Representantes asignados</option>
                                                              </select>
                                                          </div>
                                                          <div class="form-group borderCuadro subirBox">
                                                              <label for="ejemplo_archivo_1">
                                                                  <span class="icon-file-text2 subirIncono"></span>
                                                              </label>
                                                              <input type="file" id="ejemplo_archivo_1">
                                                              <h2 class="help-block">Cargar documento doc/pdf/jpg/png</h2>
                                                          </div>
                                                      </div>
                                                  </div>
                                                  <div class="modal-footer">
                                                    <button class="btn btn-success" type="button" name="button" data-dismiss="modal" aria-label="close">Compartir</button>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                             <!-- Modal- Nueva notif -->

                    </div>
                </div><!--container-->
        </div>
        <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->


    <script>
        $(document).ready(function () {
            // Seccion active (MENU)
            $(document).ready(function () {
                $('#ninos').addClass('activo');
                $('#ninosli').addClass('activoli');
            });

            $(document).ready(function() {
                $('#representantes').DataTable();
            } );

        });
    </script>

<?php footer(); ?>
