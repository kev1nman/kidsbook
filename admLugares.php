<?php require_once("master.php"); cabecera(); ?>
    <div id='wrapper' class='toggled'>
        <?php sideBar(); ?>

        <!-- Page Content -->
        <div id='page-content-wrapper'>
            <?php topBar(); ?>
                <div class='container-fluid'>
                    <div class='row'>
                        <div class='col-md-12 noP fila'>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-7 noP">
                                    <h2 class='titulo2'>Lugares de interes</h2>
                                </div>
                                <div class="col-xs-5 noP">
                                    <img class="imgTtitulo" src="img/lugares.png" alt="">
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-12 recuadroInfo sombra noP">
                                    <img class="imgRecuadro" src="img/ninos.png" alt="">
                                    <h3 class="titulo3">
                                        Niños <br>
                                        <small>542</small>
                                    </h3>
                                    <img class="imgRecuadro2" src="img/mundo.png" alt="">
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-12 recuadroInfo sombra noP">
                                    <img class="imgRecuadro" src="img/representantes.png" alt="">
                                    <h3 class="titulo3">
                                        Representantes <br>
                                        <small>854</small>
                                    </h3>
                                    <img class="imgRecuadro2" src="img/mundo.png" alt="">
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-12 recuadroInfo sombra noP">
                                    <img class="imgRecuadro" src="img/colegios.png" alt="">
                                    <h3 class="titulo3">
                                        Colegios <br>
                                        <small>19</small>
                                    </h3>
                                    <img class="imgRecuadro2" src="img/mundo.png" alt="">
                                </div>
                            </div>


                        </div>

                        <div class="col-xs-12 noP">
                            <div class="panel panel-default">
                                <!-- Default panel contents -->
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-md-12 addNewBtn">
                                            <a class="btn btn-success" href="admLugaresNuevo.php">Agregar lugares</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <button type="button" class="btn btn-default disabled" name="button"><span class="glyphicon glyphicon-trash"></span> Borrar seleccionados</button>
                                </div>

                                <div class="table-responsive">
                                    <!-- Table -->
                                    <table id="lugares" class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th>Proveedor</th>
                                                <th>Teléfono</th>
                                                <th>Correo</th>
                                                <th>Tipo</th>
                                                <th>Ubicación</th>
                                                <th>Acciones</th>
                                            </tr>

                                        </thead>
                                        <tr>
                                            <td class="checking"><input type="checkbox" name="" value=""></td>
                                            <td class="checking"></td>
                                            <td> Nombre del proveedor</td>
                                            <td>02436653632</td>
                                            <td>colegio@correo.com</td>
                                            <td>Comida</td>
                                            <td>Caracas, Venezuela</td>
                                            <td>

                                                <span class="glyphicon glyphicon-trash iconTable"></span>
                                                <span class="glyphicon glyphicon-eye-open iconTable"></span>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="checking"><input type="checkbox" name="" value=""></td>
                                            <td class="checking"></td>
                                            <td> Nombre del proveedor</td>
                                            <td>02436653632</td>
                                            <td>colegio@correo.com</td>
                                            <td>Comida</td>
                                            <td>Caracas, Venezuela</td>
                                            <td>

                                                <span class="glyphicon glyphicon-trash iconTable"></span>
                                                <span class="glyphicon glyphicon-eye-open iconTable"></span>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="checking"><input type="checkbox" name="" value=""></td>
                                            <td class="checking"></td>
                                            <td> Manuel</td>
                                            <td>02436653632</td>
                                            <td>colegio@correo.com</td>
                                            <td>Comida</td>
                                            <td>Caracas, Venezuela</td>
                                            <td>

                                                <span class="glyphicon glyphicon-trash iconTable"></span>
                                                <span class="glyphicon glyphicon-eye-open iconTable"></span>

                                            </td>
                                        </tr>

                                    </table>
                                </div>
                                <div class="panel-body">
                                    <button type="button" class="btn btn-default disabled" name="button"><span class="glyphicon glyphicon-trash"></span> Borrar seleccionados</button>
                                </div>
                            </div>
                        </div>


                    </div>
                </div><!--container-->
        </div>
        <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->


    <script>
        $(document).ready(function () {
            // Seccion active (MENU)
            $(document).ready(function () {
                $('#publicid').addClass('activo');
                $('#publicidli').addClass('activoli');
            });

            $(document).ready(function() {
                $('#lugares').DataTable();
            } );

        });
    </script>

<?php footer(); ?>
