<?php require_once("master.php"); cabecera(); ?>
    <div id='wrapper' class='toggled'>
        <?php sideBar(); ?>

        <!-- Page Content -->
        <div id='page-content-wrapper'>
            <?php topBar(); ?>
                <div class='container-fluid'>
                    <div class='row'>
                        <div class='col-md-12 noP fila'>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-7 noP">
                                    <h2 class='titulo2'>Estadísticas globales</h2>
                                </div>
                                <div class="col-xs-5 noP">
                                    <img class="imgTtitulo" src="img/estadisticas.png" alt="">
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-12 recuadroInfo sombra noP">
                                    <img class="imgRecuadro" src="img/ninos.png" alt="">
                                    <h3 class="titulo3">
                                        Niños <br>
                                        <small>542</small>
                                    </h3>
                                    <img class="imgRecuadro2" src="img/mundo.png" alt="">
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-12 recuadroInfo sombra noP">
                                    <img class="imgRecuadro" src="img/representantes.png" alt="">
                                    <h3 class="titulo3">
                                        Representantes <br>
                                        <small>854</small>
                                    </h3>
                                    <img class="imgRecuadro2" src="img/mundo.png" alt="">
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-12 recuadroInfo sombra noP">
                                    <img class="imgRecuadro" src="img/colegios.png" alt="">
                                    <h3 class="titulo3">
                                        Colegios <br>
                                        <small>19</small>
                                    </h3>
                                    <img class="imgRecuadro2" src="img/mundo.png" alt="">
                                </div>
                            </div>

                        </div>

                        <div class='col-xs-12 noP fila'>
                            <div class="col-md-4">
                                <div id="grafico1" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                            </div>
                            <div class="col-md-6">
                                <div id="grafico2" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                            </div>

                            <div class="col-md-2">
                                <div class="col-xs-12 masUsuarios sombra noP">
                                    <div class="col-sm-4 col-lg-12 imagenMasUsuarios">
                                        <img src="img/masUsuarios.png" alt="">
                                    </div>

                                    <div class="col-sm-8 col-md-12 datosMasUsuarios">
                                        <h2 class="text-warning text-left">19
                                        <small class="text-warning">Colegios afiliados</small></h2>

                                        <h2 class="text-success text-left">10
                                        <small class="text-success">Activos</small></h2>

                                        <h2 class="text-danger text-left">09
                                        <small class="text-danger">Inactivos</small></h2>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class='col-xs-12 noP fila'>
                            <div class="col-md-8 noP">
                                <div id="grafico3" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                            </div>
                            <div class="col-xs-12 col-md-4">

                                <div class="col-xs-12 usuariosON sombra">
                                    <div class="col-xs-12 noP">
                                        <div class="col-xs-12">
                                            <h4>Conectados actualmente</h4>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 noP">
                                        <div class="media">
                                            <div class="media-left media-middle">
                                                <a href="#">
                                                    <img class="media-object img-circle" src="img/colegios.png">
                                                </a>
                                            </div>
                                            <div class="media-body media-middle">
                                                <h5 class="media-heading">Nombre del colegio</h5>
                                            </div>
                                        </div>

                                        <div class="media">
                                            <div class="media-left media-middle">
                                                <a href="#">
                                                    <img class="media-object img-circle" src="img/colegios.png">
                                                </a>
                                            </div>
                                            <div class="media-body media-middle">
                                                <h5 class="media-heading">Nombre del colegio</h5>
                                            </div>
                                        </div>

                                        <div class="media">
                                            <div class="media-left media-middle">
                                                <a href="#">
                                                    <img class="media-object img-circle" src="img/colegios.png">
                                                </a>
                                            </div>
                                            <div class="media-body media-middle">
                                                <h5 class="media-heading">Nombre del colegio</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--container-->
        </div>
        <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->


    <script>
        $(document).ready(function () {
            // Seccion active (MENU)
            $(document).ready(function () {
                $('#estadisticas').addClass('activo');
                $('#estadisticasli').addClass('activoli');
            });

            // Grafico 1 estilo PIE
            Highcharts.chart('grafico1', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: null
                },
                tooltip: {
                    pointFormat: '<b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }
                },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data: [{
                        name: 'Niños',
                        y: 56.33
                    }, {
                        name: 'Representantes',
                        y: 24.03,
                        sliced: true,
                        selected: true
                    }, {
                        name: 'Maestros',
                        y: 10.38
                    }]
                }]
            });

            // Grafico 2 estilo LINEAL
            Highcharts.chart('grafico2', {
                chart: {
                    type: 'line'
                },
                title: {
                    text: 'Rendimiento mensual'
                },
                xAxis: {
                    categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
                },
                yAxis: {
                    title: {
                        text: null
                    }
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
                series: [{
                    name: '2017',
                    data: [7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
                }, {
                    name: '2016',
                    data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
                }]
            });

            // Grafico 3 estilo COLUMNAS
            Highcharts.chart('grafico3', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Colegios afiliados por zona'
                },
                xAxis: {
                    type: 'category',
                    labels: {
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Population (millions)'
                    }
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: '<b>{point.y:.1f} millions</b>'
                },
                series: [{
                    name: 'Population',
                    data: [
                        ['Shanghai', 23.7],
                        ['Lagos', 16.1],
                        ['Istanbul', 14.2],
                        ['Karachi', 14.0],
                        ['Mumbai', 12.5],
                        ['Moscow', 12.1],
                        ['São Paulo', 11.8],
                        ['Beijing', 11.7],
                        ['Guangzhou', 11.1],
                        ['Delhi', 11.1],
                        ['Shenzhen', 10.5],
                        ['Seoul', 10.4],
                        ['Jakarta', 10.0],
                        ['Kinshasa', 9.3],
                        ['Tianjin', 9.3],
                        ['Tokyo', 9.0],
                        ['Cairo', 8.9],
                        ['Dhaka', 8.9],
                        ['Mexico City', 8.9],
                        ['Lima', 8.9]
                    ],
                    dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#FFFFFF',
                        align: 'right',
                        format: '{point.y:.1f}', // one decimal
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                }]
            });
        });
    </script>

<?php footer(); ?>
