<?php require_once("master.php"); cabecera(); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="franjaLeft"></div>
            <div class="franjaTop"></div>
            <div class="col-md-12">
                <form>
                    <div class="col-md-12 text-center vistaLogin">
                        <div class="row">
                            <div class="col-sm-4 col-md-4 btnIdioma3">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <img class="img-circle idioma" src="img/espanol.jpg">
                                        <span class="glyphicon glyphicon-chevron-down"></span>
                                    </button>
                                    
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="#"><img class="img-circle img-resposive idioma" src="img/espanol.jpg"></a>
                                        </li>
                                        <li>
                                            <a href="#"><img class="img-circle img-resposive idioma" src="img/ingles.jpg"></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="col-md-8 text-left">
                                <h1>
                                    <strong class="titulo1">Bienvenido a </strong><strong class="titulo2">Kids Book</strong>
                                </h1>
                                
                            </div>
                            <div class="col-md-12">
                                <h2>Iniciar seción</h2> <br><br>
                            </div>
                        </div>

                        <div class="col-md-4 col-md-offset-4">

                            <div class="form-group">
                                <input type="email" class="form-control loginInput" id="exampleInputEmail1" placeholder="Email">
                            </div>

                            <div class="form-group">
                                <input type="password" class="form-control loginInput" id="exampleInputPassword1" placeholder="Password">
                            </div>

                            <button class="btn btn-success" type="submit" name="button">Iniciar seción</button> <br><br>
                            <p><a class="text-danger" href="recuperarPass.php">¿ Olvidaste tu usuario o contraseña ? <span class="icon-loop2"></span></a></p>
                        </div>

                        <div class="col-md-12">
                            <h3>Botones de prueba</h3>
                            <a class="btn btn-warning" href="admEstadisticas.php">Administrador</a>
                            <a class="btn btn-danger" href="Maestro/profNotificacion.php">Maestro</a>
                            <a class="btn btn-info" href="director/dirNinos.php">Director</a>
                        </div>


                    </div>

                </form>
            </div>
        </div>
    </div>
<?php footer(); ?>
