<?php require_once("master.php"); cabecera(); ?>
    <div id='wrapper' class='toggled'>
        <?php sideBar(); ?>

        <!-- Page Content -->
        <div id='page-content-wrapper'>
            <?php topBar(); ?>
                <div class='container-fluid'>
                    <div class='row'>
                        <div class='col-md-12 noP fila'>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-7 noP">
                                    <h2 class='titulo2'>Recopiladores</h2>
                                </div>
                                <div class="col-xs-5 noP">
                                    <img class="imgTtitulo" src="img/recopiladores.png" alt="">
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-12 recuadroInfo sombra noP">
                                    <img class="imgRecuadro" src="img/ninos.png" alt="">
                                    <h3 class="titulo3">
                                        Niños <br>
                                        <small>542</small>
                                    </h3>
                                    <img class="imgRecuadro2" src="img/mundo.png" alt="">
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-12 recuadroInfo sombra noP">
                                    <img class="imgRecuadro" src="img/representantes.png" alt="">
                                    <h3 class="titulo3">
                                        Representantes <br>
                                        <small>854</small>
                                    </h3>
                                    <img class="imgRecuadro2" src="img/mundo.png" alt="">
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-12 recuadroInfo sombra noP">
                                    <img class="imgRecuadro" src="img/colegios.png" alt="">
                                    <h3 class="titulo3">
                                        Colegios <br>
                                        <small>19</small>
                                    </h3>
                                    <img class="imgRecuadro2" src="img/mundo.png" alt="">
                                </div>
                            </div>


                        </div>

                        <div class="col-xs-12 noP">
                            <div class="panel panel-default">
                                <!-- Default panel contents -->
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-md-12 addNewBtn">
                                            <a class="btn btn-success" href="admRecopiladoresNuevo.php">Agregar recopiladores</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <button type="button" class="btn btn-default disabled" name="button"><span class="glyphicon glyphicon-trash"></span> Borrar seleccionados</button>
                                </div>

                                <div class="table-responsive">
                                    <!-- Table -->
                                    <table id="recopiladores" class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th>Nombre</th>
                                                <th>Apellido</th>
                                                <th>Teléfono</th>
                                                <th>Correo</th>
                                                <th>Acciones</th>
                                            </tr>

                                        </thead>
                                        <tr>
                                            <td class="checking"><input type="checkbox" name="" value=""></td>
                                            <td class="checking"><span class="icon-star-full text-warning starColor"></span></td>
                                            <td> Maria</td>
                                            <td>Correa</td>
                                            <td>5226779988787</td>
                                            <td>colegio@correo.com</td>
                                            <td>

                                                <span class="glyphicon glyphicon-trash iconTable"></span>
                                                <span class="glyphicon glyphicon-eye-open iconTable"></span>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="checking"><input type="checkbox" name="" value=""></td>
                                            <td class="checking"><span class="icon-star-full text-warning starColor"></span></td>
                                            <td> Maria</td>
                                            <td>Correa</td>
                                            <td>5226779988787</td>
                                            <td>colegio@correo.com</td>
                                            <td>

                                                <span class="glyphicon glyphicon-trash iconTable"></span>
                                                <span class="glyphicon glyphicon-eye-open iconTable"></span>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="checking"><input type="checkbox" name="" value=""></td>
                                            <td class="checking"><span class="icon-star-full text-warning starColor"></span></td>
                                            <td> Maria</td>
                                            <td>Correa</td>
                                            <td>5226779988787</td>
                                            <td>colegio@correo.com</td>
                                            <td>

                                                <span class="glyphicon glyphicon-trash iconTable"></span>
                                                <span class="glyphicon glyphicon-eye-open iconTable"></span>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="checking"><input type="checkbox" name="" value=""></td>
                                            <td class="checking"><span class="icon-star-full text-warning starColor"></span></td>
                                            <td> Manuel</td>
                                            <td>Correa</td>
                                            <td>5226779988787</td>
                                            <td>colegio@correo.com</td>
                                            <td>

                                                <span class="glyphicon glyphicon-trash iconTable"></span>
                                                <span class="glyphicon glyphicon-eye-open iconTable"></span>

                                            </td>
                                        </tr>


                                    </table>
                                </div>
                                <div class="panel-body">
                                    <button type="button" class="btn btn-default disabled" name="button"><span class="glyphicon glyphicon-trash"></span> Borrar seleccionados</button>
                                </div>
                            </div>
                        </div>


                    </div>
                </div><!--container-->
        </div>
        <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->


    <script>
        $(document).ready(function () {
            // Seccion active (MENU)
            $(document).ready(function () {
                $('#publicid').addClass('activo');
                $('#publicidli').addClass('activoli');
            });

            $(document).ready(function() {
                $('#recopiladores').DataTable();
            } );

        });
    </script>

<?php footer(); ?>
