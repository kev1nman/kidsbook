<?php require_once("master.php"); cabecera(); ?>
    <div id='wrapper' class='toggled'>
        <?php sideBar(); ?>

        <!-- Page Content -->
        <div id='page-content-wrapper'>
            <?php topBar(); ?>
                <div class='container-fluid'>
                    <div class='row'>
                        <div class='col-md-12 noP fila'>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-7 noP">
                                    <h2 class='titulo2'>Categorias</h2>
                                </div>
                                <div class="col-xs-5 noP">
                                    <img class="imgTtitulo" src="img/categoria.png" alt="">
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-12 recuadroInfo sombra noP">
                                    <img class="imgRecuadro" src="img/ninos.png" alt="">
                                    <h3 class="titulo3">
                                        Niños <br>
                                        <small>542</small>
                                    </h3>
                                    <img class="imgRecuadro2" src="img/mundo.png" alt="">
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-12 recuadroInfo sombra noP">
                                    <img class="imgRecuadro" src="img/representantes.png" alt="">
                                    <h3 class="titulo3">
                                        Representantes <br>
                                        <small>854</small>
                                    </h3>
                                    <img class="imgRecuadro2" src="img/mundo.png" alt="">
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-12 recuadroInfo sombra noP">
                                    <img class="imgRecuadro" src="img/colegios.png" alt="">
                                    <h3 class="titulo3">
                                        Colegios <br>
                                        <small>19</small>
                                    </h3>
                                    <img class="imgRecuadro2" src="img/mundo.png" alt="">
                                </div>
                            </div>

                        </div>

                        <div class="col-xs-12 noP">
                            <div class="panel panel-default">
                                <!-- Default panel contents -->
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-md-4 addNewBtn">
                                            <a class="btn btn-success" href="admCategoriasNuevo.php">Agregar categoría</a>
                                        </div>
                                        <div class='col-xs-10 col-sm-6 boxSearch boxSearch2'>
                                            <div class='input-group'>
                                                <span class='input-group-btn'>
                                                    <button class='btn btn-default btnSearch' type='button'><span class='glyphicon glyphicon-search'></span></button>
                                                </span>
                                                <input type='text' class='form-control searchMenu2' placeholder='Buscar colegios...'>
                                            </div>
                                            <!-- /input-group -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <div class="col-md-4">
                                    <div class="fondoCategoria comida">
                                        <div class="boxImgCategoria">
                                            <img class="imgCategoria" src="img/burger.jpg" alt="">
                                        </div>
                                        <h2 class="comidaT">Comida rápida</h2>
                                        <div class="btnCategoria">
                                            <span class="glyphicon glyphicon-pencil"></span>
                                            <span class="glyphicon glyphicon-trash iconTable"></span>
                                            <span class="glyphicon glyphicon-eye-open iconTable"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="fondoCategoria contadores">
                                        <div class="boxImgCategoria">
                                            <img class="imgCategoria" src="img/boss.jpg" alt="">
                                        </div>
                                        <h2 class="contadoresT">Contadores y asesores</h2>
                                        <div class="btnCategoria">
                                            <span class="glyphicon glyphicon-pencil"></span>
                                            <span class="glyphicon glyphicon-trash iconTable"></span>
                                            <span class="glyphicon glyphicon-eye-open iconTable"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="fondoCategoria desarrollo">
                                        <div class="boxImgCategoria">
                                            <img class="imgCategoria" src="img/koala.jpg" alt="">
                                        </div>
                                        <h2 class="desarrolloT">Desarrollo de aplicaciones -Kidsbook-</h2>
                                        <div class="btnCategoria">
                                            <span class="glyphicon glyphicon-pencil"></span>
                                            <span class="glyphicon glyphicon-trash iconTable"></span>
                                            <span class="glyphicon glyphicon-eye-open iconTable"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="fondoCategoria pediatras">
                                        <div class="boxImgCategoria">
                                            <img class="imgCategoria" src="img/pediatras.jpg" alt="">
                                        </div>
                                        <h2 class="pediatrasT">Pediatras</h2>
                                        <div class="btnCategoria">
                                            <span class="glyphicon glyphicon-pencil"></span>
                                            <span class="glyphicon glyphicon-trash iconTable"></span>
                                            <span class="glyphicon glyphicon-eye-open iconTable"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="fondoCategoria proximamente">
                                        <div class="boxImgCategoria">
                                            <img class="imgCategoria" src="img/favorito.jpg" alt="">
                                        </div>
                                        <h2 class="proximamenteT">Próximamente</h2>
                                        <div class="btnCategoria">
                                            <span class="glyphicon glyphicon-pencil"></span>
                                            <span class="glyphicon glyphicon-trash iconTable"></span>
                                            <span class="glyphicon glyphicon-eye-open iconTable"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="fondoCategoria tiendas">
                                        <div class="boxImgCategoria">
                                            <img class="imgCategoria" src="img/tiendas.jpg" alt="">
                                        </div>
                                        <h2 class="tiendasT">Tiendas por departamento</h2>
                                        <div class="btnCategoria">
                                            <span class="glyphicon glyphicon-pencil"></span>
                                            <span class="glyphicon glyphicon-trash iconTable"></span>
                                            <span class="glyphicon glyphicon-eye-open iconTable"></span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>


                    </div>
                </div><!--container-->
        </div>
        <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->


    <script>
        $(document).ready(function () {
            // Seccion active (MENU)
            $(document).ready(function () {
                $('#publicid').addClass('activo');
                $('#publicidli').addClass('activoli');
            });

        });
    </script>

<?php footer(); ?>
