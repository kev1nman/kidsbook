<?php require_once("master.php"); cabecera(); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="franjaLeft"></div>
            <div class="franjaTop"></div>
            <div class="col-md-12">
                <form>
                    <div class="col-md-12 text-center vistaLogin">
                        <div class="col-md-12 noP">
                            <div class="col-md-4 noP">
                                <button class="btn btn-default dropdown-toggle btnIdioma2" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <img class="img-circle idioma" src="img/ingles.jpg" alt="">
                                    <span class="caret"></span>
                                </button>

                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    <li><a href="#">Spanish</a></li>
                                    <li><a href="#">English</a></li>
                                </ul>
                            </div>

                            <div class="col-md-8 text-left">
                                <h1>
                                    <strong class="titulo1">Recuperar contraseña </strong>
                                </h1>
                            </div>
                            <div class="col-md-12">
                                <h2>Ingresa tu correo</h2> <br><br>
                            </div>
                        </div>

                        <div class="col-md-4 col-md-offset-4">

                            <div class="form-group">
                                <input type="email" class="form-control loginInput" id="exampleInputEmail1" placeholder="Email">
                            </div>
                            <button class="btn btn-info" type="submit" name="button">Enviar código de recuperación</button> <br><br>
                            <p>Recibiras un código en tu correo electrónico, presiona <strong>Continuar</strong> sólo cuando hayas recibido el correo, en caso contrario, <a href="#">reenvia un nuevo código</a></p>
                            <a class="btn btn-success" href="recuperarPassCodigo.php">Continuar</a>
                        </div>



                    </div>

                </form>
            </div>
        </div>
    </div>
<?php footer(); ?>
