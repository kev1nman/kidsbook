<?php
error_reporting(E_ALL ^ E_NOTICE);
function cabecera(){echo "
    <!DOCTYPE html>
    <html lang='en'>

    <head>
        <meta charset='UTF-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1.0'>
        <meta http-equiv='X-UA-Compatible' content='ie=edge'>

        <!--CSS-->
        <link rel='stylesheet' href='css/bootstrap.min.css'>
        <link rel='stylesheet' href='css/IconStyle.css'>
        <link rel='stylesheet' href='css/animate.min.css'>
        <link rel='stylesheet' href='css/dataTables.bootstrap.css'>
        <link rel='stylesheet' href='css/bootstrap-multiselect.css'>
        <link rel='stylesheet' href='css/style.css'>
        <link rel='stylesheet' href='css/bootstrap-datetimepicker.min.css'>

        <!--CSS - THEME -->
        <link rel='stylesheet' href='css/tema.css'>

        <!--FAVICON-->
        <link rel='shortcut icon' href='img/favicon.ico' type='image/x-icon'>
        <link rel='icon' href='img/favicon.ico' type='image/x-icon'>

        <!--JS-->
        <script src='js/jquery.js' type='text/javascript'></script>
        <script src='js/bootstrap.min.js' type='text/javascript'></script>
        <script src='js/menu.js' type='text/javascript'></script>
        <script src='js/highcharts.js' type='text/javascript'></script>
        <script src='js/dataTables.js' type='text/javascript'></script>
        <script src='js/dataTables.bootstrap.js' type='text/javascript'></script>
        <script src='js/bootstrap-multiselect.js' type='text/javascript'></script>
        <script src='js/moment.min.js'></script>
        <script src='js/bootstrap-datetimepicker.min.js'></script>
        <script src='js/bootstrap-datetimepicker.es.js'></script>
        <script language='javascript' src='js/scrollbars.js'></script>

        
        <title>Kidsbook</title>
    </head>

    <body>

";}
function sideBar(){echo "
    <!-- Sidebar -->
    <nav class='navbar navbar-inverse navbar-fixed-top' id='sidebar-wrapper' role='navigation'>
        <ul class='nav sidebar-nav'>
            <li class='sidebar-brand brand' title='' anything='Editar perfil'>
                <a href='admPerfil.php' class='perfil'>
                    <img src='img/koala.jpg' class='iconoPerfil'>
                    <h4>José</h4>
                    <p>Administrador</p>
                </a>
            </li>
            <li id='colegioli' title='' anything='Colegio'>
                <a href='admColegio.php' class='' id='colegio'><img src='img/colegio.png' class='iconoMenu' alt=''></a>
            </li>
            <li id='notificacionli' title='' anything='Notificación'>
                <a href='admNotificacion.php' class='' id='notificacion'><img src='img/notificacion.png' class='iconoMenu' alt=''></a>
            </li>
            <li id='estadisticasli' title='' anything='Estadísticas'>
                <a href='admEstadisticas.php' class='' id='estadisticas'><img src='img/estadisticas.png' class='iconoMenu' alt=''></a>
            </li>
            <li id='publicidli' title='' anything='Publicidad'>
                <a href='admPublicidadGestion.php' class='' id='publicid'><img src='img/publicid.png' class='iconoMenu' alt=''></a>
            </li>
        </ul>
    </nav>
    <!-- /#sidebar-wrapper -->

";}
function topBar(){echo "
    <button type='button' class='hamburger is-closed' data-toggle='offcanvas' id='hamburger'>
        <span class='hamb-top'></span>
        <span class='hamb-middle'></span>
        <span class='hamb-bottom'></span>
    </button>


    <nav class='navbar navbar-inverse header'>
        <div class='container'>
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class='navbar-header'>
                <button type='button' class='navbar-toggle collapsed' data-toggle='collapse' data-target='#bs-example-navbar-collapse-1' aria-expanded='false'>
                    <span class='sr-only'>Toggle navigation</span>
                    <span class='glyphicon glyphicon-option-vertical'></span>
                </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class='collapse navbar-collapse' id='bs-example-navbar-collapse-1'>
                <div class='col-md-10 topBar'>
                    <div class='col-xs-2 dropdown noP btnIdioma3'>
                        <div class='btn-group'>
                            <button type='button' class='btn btn-default dropdown-toggle btnIdioma' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                                <img class='img-circle idioma' src='img/espanol.jpg'>
                                <span class='glyphicon glyphicon-chevron-down'></span>
                            </button>
                            
                            <ul class='dropdown-menu'>
                                <li>
                                    <a href='#'><img class='img-circle img-resposive idioma' src='img/espanol.jpg'></a>
                                </li>
                                <li>
                                    <a href='#'><img class='img-circle img-resposive idioma' src='img/ingles.jpg'></a>
                                </li>
                            </ul>
                        </div>
                    </div>


                    <div class='col-xs-10 col-sm-6 boxSearch'>
                        <div class='input-group'>
                            <span class='input-group-btn'>
                                <button class='btn btn-default btnSearch' type='button'><span class='glyphicon glyphicon-search'></span></button>
                            </span>
                            <input type='text' class='form-control searchMenu' placeholder='Search for...'>
                        </div>
                        <!-- /input-group -->
                    </div>


                </div>
                <!-- /.row -->

            </div>
            <!-- /.navbar-collapse -->
            <div class='col-xs-12 col-sm-4 text-center botonesRight noP'>
                <div class='col-xs-3 dropdown noP'>
                    <button title='' anything='Notificaciones' class='btn btn-default dropdown-toggle btnNotif' type='button' id='dropdownMenu1' data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'>
                        <img src='img/mensaje.png' alt=''>
                        <div class='numberNotif'>
                            <p>3</p>
                        </div>
                    </button>

                    <ul class='dropdown-menu boxAllNotif' aria-labelledby='dropdownMenu1'>
                        <li><p>Tienes 3 nuevos mensajes</p></li>
                        <li>
                            <a class='boxNotif' href='#'>
                                <p class='tiempoNotif'>Hace 2 horas</p>
                                <p class='quienEnvia'>Nombre de quien envia</p>
                                <p class='tituloNotif'>Tìtulo del mensaje</p>
                            </a>
                        </li>
                        <li>
                            <a class='boxNotif' href='#'>
                                <p class='quienEnvia'>Nombre de quien envia</p>
                                <p class='tiempoNotif'>Hace 2 horas</p>
                                <p class='tituloNotif'>Tìtulo del mensaje</p>
                            </a>
                        </li>
                        <li>
                            <a class='boxNotif' href='#'>
                                <p class='quienEnvia'>Nombre de quien envia</p>
                                <p class='tiempoNotif'>Hace 2 horas</p>
                                <p class='tituloNotif'>Tìtulo del mensaje</p>
                            </a>
                        </li>
                        <li><a class='btn verTodas' href='#'>Ver todas las notificaciones</a></li>
                    </ul>
                </div>
                <div class='col-xs-3 dropdown noP'>
                    <button title='' anything='Notificaciones' class='btn btn-default dropdown-toggle btnNotif' type='button' id='dropdownMenu1' data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'>
                        <img src='img/notificacion.png' alt=''>
                        <div class='numberNotif'>
                            <p>3</p>
                        </div>
                    </button>

                    <ul class='dropdown-menu boxAllNotif' aria-labelledby='dropdownMenu1'>
                        <li><p>Tienes 3 nuevas notificaciones</p></li>
                        <li>
                            <a class='boxNotif' href='#'>
                                <p class='tiempoNotif'>Hace 2 horas</p>
                                <p class='quienEnvia'>Nombre de quien envia</p>
                                <p class='tituloNotif'>Tìtulo del mensaje</p>
                            </a>
                        </li>
                        <li>
                            <a class='boxNotif' href='#'>
                                <p class='quienEnvia'>Nombre de quien envia</p>
                                <p class='tiempoNotif'>Hace 2 horas</p>
                                <p class='tituloNotif'>Tìtulo del mensaje</p>
                            </a>
                        </li>
                        <li>
                            <a class='boxNotif' href='#'>
                                <p class='quienEnvia'>Nombre de quien envia</p>
                                <p class='tiempoNotif'>Hace 2 horas</p>
                                <p class='tituloNotif'>Tìtulo del mensaje</p>
                            </a>
                        </li>
                        <li><a class='btn verTodas' href='#'>Ver todas las notificaciones</a></li>
                    </ul>
                </div>
                <div class='col-xs-3 dropdown noP'>
                    <button title='' anything='Temas' class='btn btn-default dropdown-toggle btnNotif' type='button' id='dropdownMenu1' data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'>
                            <img src='img/temas.png' alt=''>
                        </button>
                    <ul class='dropdown-menu boxAllThemes' aria-labelledby='dropdownMenu1'>
                        <li><p>Cambiar tema</p></li>
                        <li>
                            <a class='boxNotif' href='#'>
                                <p class='tituloNotif'>Tema 1</p>
                            </a>
                        </li>
                        <li>
                            <a class='boxNotif' href='#'>
                                <p class='tituloNotif'>Tema 2</p>
                            </a>
                        </li>
                        <li>
                            <a class='boxNotif' href='#'>
                                <p class='tituloNotif'>Tema 3</p>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class='col-xs-3 noP'>
                    <a title='' anything='Salir' class='btn btn-default btnNotif' href='index.php'>
                        <img src='img/salir.png' alt=''>
                    </a>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </nav>
";}
function footer(){echo "

    <script>
        var mediaquery = window.matchMedia('(max-width: 768px)');
        if (mediaquery.matches) {
            $('#wrapper').removeClass('toggled');
            $('#hamburger').removeClass('is-closed');
            $('#hamburger').addClass('is-open');

        } else {
            // mediaquery no es 600
        }
    </script>

    <footer class='footer'>
      <div class='container text-right'>
        <p class='text-muted'>Desarrollado por <a class='text-warning' href='#'>Lars Sfotware Company</a> (2017).</p>
      </div>
    </footer>


    </body>
    </html>
";}
