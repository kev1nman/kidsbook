<?php require_once("master.php"); cabecera(); ?>
    <div id='wrapper' class='toggled'>
        <?php sideBar(); ?>

        <!-- Page Content -->
        <div id='page-content-wrapper'>
            <?php topBar(); ?>
                <div class='container'>
                    <div class="col-md-12">
                        <form>
                            <div class="col-md-12 text-center vistaLogin noP">
                                <div class="col-md-12 noP">
                                    <div class="col-md-12">
                                        <h2>Activar publicidad <img class="imgTtitulo" src="img/publicid.png" alt=""></h2> <br>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-md-6 col-md-offset-3 text-left">
                                    <div class="agregarCategoria">
                                        <div class="fondoAgregarPublicidad">
                                            <div class="boxImgPublicidad">
                                                <label for="ejemplo_archivo_1">
                                                    <input type="file" id="ejemplo_archivo_1">
                                                    <span class="glyphicon glyphicon-paperclip imgPublicidad subirIncono"></span> <br>
                                                    <h4>Agregue imagen</h4>
                                                    <p class="help-block">La imagen debe ser cuadrada (1:1) y en formatos como jpg/png/gif.</p>
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <h3>Opciones de activación</h3>

                                    <div class="col-md-12 noP">
                                        <div class="col-md-6">
                                           <div class="form-group">
                                              <div class='input-group date datetimepicker' >
                                                <input type='text' placeholder="Fecha Desde"  class="sinB" />
                                                  <span class="input-group-addon">
                                                     <span class="glyphicon glyphicon-calendar"></span>
                                                 </span>
                                              </div>
                                            </div>

                                            <div class="form-group">
                                                <select class="form-control loginInput" name="">
                                                    <option value="">Pais</option>
                                                    <option value="">Pais 1</option>
                                                    <option value="">Pais 2</option>
                                                    <option value="">Pais 3</option>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <select class="form-control loginInput" name="">
                                                    <option value="">Colegio</option>
                                                    <option value="">Colegio 1</option>
                                                    <option value="">Colegio 2</option>
                                                    <option value="">Colegio 3</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                              <div class='input-group date datetimepicker'>
                                                <input type='text' placeholder="Fecha Hasta"  class="sinB" />
                                                  <span class="input-group-addon">
                                                     <span class="glyphicon glyphicon-calendar"></span>
                                                 </span>
                                              </div>
                                         </div>

                                            <div class="form-group">
                                                <select class="form-control loginInput" name="">
                                                    <option value="">Ciudad</option>
                                                    <option value="">Ciudad 1</option>
                                                    <option value="">Ciudad 2</option>
                                                    <option value="">Ciudad 3</option>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <select class="form-control loginInput" name="">
                                                    <option value="">Grupo</option>
                                                    <option value="">Grupo 1</option>
                                                    <option value="">Grupo 2</option>
                                                    <option value="">Grupo 3</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="1">Pantalla 1</label><input id="1" type="checkbox" name="" value="" class="checking">
                                                </div>

                                                <div class="form-group">
                                                    <label for="2">Pantalla 2</label><input id="2" type="checkbox" name="" value="" class="checking">
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="3">Pantalla 3</label><input id="3" type="checkbox" name="" value="" class="checking">
                                                </div>

                                                <div class="form-group">
                                                    <label for="4">Pantalla 4</label><input id="4" type="checkbox" name="" value="" class="checking">
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="5">Pantalla 5</label><input id="5" type="checkbox" name="" value="" class="checking">
                                                </div>

                                                <div class="form-group">
                                                    <label for="6">Pantalla 6</label><input id="6" type="checkbox" name="" value="" class="checking">
                                                </div>
                                            </div>



                                        </div>

                                    </div>



                                </div>



                                <div class="col-xs-12">
                                    <br><br>
                                    <a class="btn btn-success" href="#">Guardar</a>
                                    <a class="btn btn-warning" href="admPublicidad.php">Volver</a>
                                </div>


                            </div>

                        </form>
                    </div>
                </div><!--container-->
        </div>
        <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->

    <script type="text/javascript">
            $(function () {
                $('.datetimepicker').datetimepicker();
            });
    </script>

    <script>
        $(document).ready(function () {
            // Seccion active (MENU)
            $(document).ready(function () {
                $('#publicid').addClass('activo');
                $('#publicidli').addClass('activoli');
            });

        });
    </script>

<?php footer(); ?>
