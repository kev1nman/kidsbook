<?php require_once("master.php"); cabecera(); ?>
    <div id='wrapper' class='toggled'>
        <?php sideBar(); ?>

        <!-- Page Content -->
        <div id='page-content-wrapper'>
            <?php topBar(); ?>
                <div class='container'>
                    <div class="row">
                        <div class="col-md-12 noP">
                            <form>
                                <div class="col-md-12 text-center vistaLogin">
                                    <div class="col-md-12 noP">
                                        <div class="col-md-12">
                                            <h2>Mi perfil <img src="img/koala.png" alt=""></h2> <br>
                                        </div>
                                    </div>

                                    <div class="col-md-8 col-md-offset-2 noP">
                                        <div class="col-md-6">
                                            <h3 class="titulo2">Cambiar nombre</h3>
                                            <div class="form-group">
                                                <input type="nombre" class="form-control loginInput" id="exampleInputEmail1" placeholder="Nuevo nombre de usuario (*)">
                                            </div>

                                            <div class="form-group">
                                                <input type="tel" class="form-control loginInput" id="exampleInputEmail1" placeholder="Repetir nombre (*)">
                                            </div>

                                        </div>

                                        <div class="col-md-6">
                                            <h3 class="titulo2">Cambiar contraseña</h3>
                                            <div class="form-group">
                                                <input type="email" class="form-control loginInput" id="exampleInputEmail1" placeholder="Nueva contraseña (*)">
                                            </div>

                                            <div class="form-group">
                                                <input type="email" class="form-control loginInput" id="exampleInputEmail1" placeholder="Repetir contraseña (*)">
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <p class="text-danger">(*) Campos obligatorios</p>
                                            <a class="btn btn-success" href="#">Guardar</a>
                                        </div>

                                    </div>


                                    <div class="col-md-6 col-md-offset-3 alert" role="alert">
                                        <p><span class="glyphicon glyphicon-exclamation-sign text-danger"></span> Todos los cambios son confidenciales y permanentes</p>
                                    </div>

                                </div>
                            </form>
                        </div>

                    </div>
                </div><!--container-->
        </div>
        <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->


    <script>
        $(document).ready(function () {
            // Seccion active (MENU)
            $(document).ready(function () {
                $('#perfil').addClass('activo');
                $('#perfilli').addClass('activoli');
            });

        });
    </script>

<?php footer(); ?>
