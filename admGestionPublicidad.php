<?php require_once("master.php"); cabecera(); ?>
    <div id='wrapper' class='toggled'>
        <?php sideBar(); ?>

        <!-- Page Content -->
        <div id='page-content-wrapper'>
            <?php topBar(); ?>
                <div class='container'>
                    <div class="row">
                        <div class="col-md-12 noP">
                            <form>
                                <div class="col-md-12 text-center vistaLogin">

                                    <div class="col-md-8 col-md-offset-2 noP">
                                        <div class="col-md-6">
                                            <a href="admPublicidad.php">
                                                <div class="fondoPublicidad2 bg-warning">
                                                    <img src="img/publicid.png" alt="">
                                                    <h2 class="text-warning">Publicidad</h2>
                                                </div>
                                            </a>
                                        </div>

                                        <div class="col-md-6">
                                            <a href="admRecopiladores.php">
                                                <div class="fondoPublicidad2 bg-info">
                                                    <img src="img/recopiladores.png" alt="">
                                                    <h2 class="text-info">Recopiladores</h2>
                                                </div>
                                            </a>
                                        </div>

                                        <div class="col-md-6">
                                            <a href="admLugares.php">
                                                <div class="fondoPublicidad2 bg-danger">
                                                    <img src="img/lugares.png" alt="">
                                                    <h2 class="text-danger">Lugares de interes</h2>
                                                </div>
                                            </a>
                                        </div>

                                        <div class="col-md-6">
                                            <a href="admCategorias.php">
                                                <div class="fondoPublicidad2 bg-success">
                                                    <img src="img/categoria.png" alt="">
                                                    <h2 class="text-success">Categorias</h2>
                                                </div>
                                            </a>
                                        </div>
                                    </div>

                                </div>
                            </form>
                        </div>

                    </div>
                </div><!--container-->
        </div>
        <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->


    <script>
        $(document).ready(function () {
            // Seccion active (MENU)
            $(document).ready(function () {
                $('#publicid').addClass('activo');
                $('#publicidli').addClass('activoli');
            });

        });
    </script>

<?php footer(); ?>
