<?php require_once("master.php"); cabecera(); ?>
    <div id='wrapper' class='toggled'>
        <?php sideBar(); ?>

        <!-- Page Content -->
        <div id='page-content-wrapper'>
            <?php topBar(); ?>
                <div class='container'>
                    <div class="col-md-12 noP">
                        <form>
                            <div class="col-md-12 text-center vistaLogin">
                                <div class="col-md-12 noP">
                                    <div class="col-md-12">
                                        <h2>Agregar colegio <img class="imgTtitulo" src="img/colegios.jpg" alt=""></h2> <br>
                                    </div>
                                </div>
                                <p class="text-danger">(*) Campos obligatorios</p>

                                <div class="col-md-8 col-md-offset-2 noP">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <label class="radio-inline"><input type="radio" name="optradio">Básico</label>
                                                <label class="radio-inline"><input type="radio" name="optradio">Premium</label>
                                            </div><!-- /input-group -->
                                        </div>

                                        <div class="col-md-8">
                                            <div class="form-group borderCuadro">
                                                <label for="ejemplo_archivo_1">Agregar logo del Colegio</label>
                                                <input type="file" id="ejemplo_archivo_1">
                                                <p class="help-block">La imagen debe ser cuadrada (1:1) y en formatos como jpg/png/gif.</p>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="input-group">
                                                <label for="tema1">Tema 1 </label><input name="tema" id="tema1" type="radio" class="radioInput" placeholder=""><br>
                                                <label for="tema2">Tema 2 </label><input name="tema" id="tema2" type="radio" class="radioInput" placeholder=""><br>
                                                <label for="tema3">Tema 3 </label><input name="tema" id="tema3" type="radio" class="radioInput" placeholder="">


                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Nombre del colegio (*)">
                                        </div>

                                        <div class="form-group">
                                            <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="RIF del colegio (*)">
                                        </div>

                                        <div class="form-group">
                                            <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Teléfono del colegio (*)">
                                        </div>

                                        <div class="form-group">
                                            <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Pais del colegio (*)">
                                        </div>

                                        <div class="form-group">
                                            <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Estado del colegio(*)">
                                        </div>




                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Nombre del director (*)">
                                        </div>

                                        <div class="form-group">
                                            <input type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Teléfono del director (*)">
                                        </div>

                                        <div class="form-group">
                                            <input type="email" class="form-control loginInput" id="exampleInputEmail1" placeholder="Correo del director (*)">
                                        </div>

                                        <div class="form-group">
                                            <textarea type="text" class="form-control loginInput" id="exampleInputEmail1" placeholder="Dirección del colegio (*)"></textarea>
                                        </div>

                                    </div>



                                    <div class="col-md-6">


                                    </div>

                                    <div class="col-md-12">
                                        <h3>Mapa</h3>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <a class="btn btn-success" href="#">Guardar</a>
                                    <a class="btn btn-warning" href="admColegio.php">Volver</a>
                                </div>


                            </div>

                        </form>
                    </div>
                </div><!--container-->
        </div>
        <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->


    <script>
        $(document).ready(function () {
            // Seccion active (MENU)
            $(document).ready(function () {
                $('#colegio').addClass('activo');
                $('#colegioli').addClass('activoli');
            });

        });
    </script>

<?php footer(); ?>
