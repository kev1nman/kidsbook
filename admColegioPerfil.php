<?php require_once("master.php"); cabecera(); ?>
    <div id='wrapper' class='toggled'>
        <?php sideBar(); ?>

        <!-- Page Content -->
        <div id='page-content-wrapper'>
            <?php topBar(); ?>
                <div class='container-fluid'>
                    <div class='row'>
                        <div class='col-md-12 noP fila'>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-7 noP">
                                    <h2 class='titulo2'>Perfil del colegio</h2>
                                </div>
                                <div class="col-xs-5 noP">
                                    <img class="imgTtitulo" src="img/colegio.png" alt="">
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-12 recuadroInfo sombra noP">
                                    <img class="imgRecuadro" src="img/ninos.png" alt="">
                                    <h3 class="titulo3">
                                        Niños <br>
                                        <small>542</small>
                                    </h3>
                                    <img class="imgRecuadro2" src="img/mundo.png" alt="">
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-12 recuadroInfo sombra noP">
                                    <img class="imgRecuadro" src="img/representantes.png" alt="">
                                    <h3 class="titulo3">
                                        Representantes <br>
                                        <small>854</small>
                                    </h3>
                                    <img class="imgRecuadro2" src="img/mundo.png" alt="">
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-12 recuadroInfo sombra noP">
                                    <img class="imgRecuadro" src="img/Maestros.png" alt="">
                                    <h3 class="titulo3">
                                        Maestros <br>
                                        <small>19</small>
                                    </h3>
                                    <img class="imgRecuadro2" src="img/mundo.png" alt="">
                                </div>
                            </div>

                        </div>

                        <div class="col-md-12 noP">
                            <div class="col-md-6">
                                <div class="col-md-12 cajaAzul sombra">
                                    <div class="media">
                                        <div class="media-left media-middle">
                                            <a href="#">
                                                <img class="media-object img-circle" src="img/colegios.png">
                                            </a>
                                        </div>
                                        <div class="media-body media-middle tituloColegio">
                                            <div class="col-md-8">
                                                <h3 class="media-heading">Nombre del colegio <span class="glyphicon glyphicon-pencil"></span></h3>
                                                <h4 class="media-heading">Nombre del director <span class="glyphicon glyphicon-pencil"></span></h4>
                                            </div>
                                            <div class="col-md-4 text-center">
                                                <span class="icon-star-full text-warning starActivo"></span>
                                                <h2 class="media-heading">ACTIVO</h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-12 cajaNaranja sombra">
                                    <div class="row">

                                        <div class="col-xs-12 noP">
                                            <div class="col-xs-12 divTitulo">
                                                <h4>Niños en el colegio: <b>32</b>
                                                    <a class="btn btn-success pull-right" href="#">Ver todos <span class="glyphicon glyphicon-eye-open"></span></a>
                                                </h4>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="table-responsive">

                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="2">Nombre del niño</th>
                                                            <th>Edad</th>
                                                            <th>Sexo</th>
                                                            <th>Nombre del representante</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <img class="img-circle" src="img/colegios.png">
                                                            </td>
                                                            <td>
                                                                Nombre del niño
                                                            </td>
                                                            <td>4 años</td>
                                                            <td></td>
                                                            <td>Maria Vitale</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>



                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="col-md-12 cajaAzul sombra">
                                    <form class="" action="" method="post">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <textarea class="form-control form-control2" name="name" rows="8" cols="80" placeholder="Dirección del colegio"></textarea>
                                            </div>
                                        </div>

                                        <div class="col-md-5">
                                            <div class="input-group">
                                                <input type="text" class="form-control form-control2" placeholder="RIF">
                                            </div>
                                        </div>

                                        <div class="col-md-7">
                                            <div class="input-group">
                                                <input type="text" class="form-control form-control2" placeholder="Correo electrónico">
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" class="form-control form-control2" placeholder="Teléfono del colegio">
                                            </div>
                                        </div>
                                        <div class="col-md-12 text-center">
                                            <br>
                                            <a class="btn btn-warning" href="#"><span class="icon-star-full"></span> Activar</a>
                                            <a class="btn btn-danger" href="#"><span class="icon-info"></span> Suspender</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-12 fila">
                            <div class="col-md-12 text-center">
                                <a class="btn btn-warning" href="admColegio.php">Volver</a>
                                <a class="btn btn-success" href="#">Guardar cambios</a>
                            </div>
                            <div class="col-md-6">
                                <div id="grafico1" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>
                            </div>
                            <div class="col-md-6">
                                <div id="grafico2" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                            </div>

                        </div>




                    </div>
                </div><!--container-->
        </div>
        <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->


    <script>
        $(document).ready(function () {
            // Seccion active (MENU)
            $(document).ready(function () {
                $('#colegio').addClass('activo');
                $('#colegioli').addClass('activoli');
            });


            // Grafico1 estilo barras horizontales
            Highcharts.chart('grafico1', {
                chart: {
                    type: 'bar'
                },
                title: {
                    text: 'Stacked bar chart'
                },
                xAxis: {
                    categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Total fruit consumption'
                    }
                },
                legend: {
                    reversed: true
                },
                plotOptions: {
                    series: {
                        stacking: 'normal'
                    }
                },
                series: [{
                    name: 'John',
                    data: [5, 3, 4, 7, 2]
                }, {
                    name: 'Jane',
                    data: [2, 2, 3, 2, 1]
                }, {
                    name: 'Joe',
                    data: [3, 4, 4, 2, 5]
                }]
            });


            // Grafico2 estico semicirculo
            Highcharts.chart('grafico2', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: 0,
                    plotShadow: false
                },
                title: {
                    text: 'Browser<br>shares<br>2015',
                    align: 'center',
                    verticalAlign: 'middle',
                    y: 40
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: true,
                            distance: -50,
                            style: {
                                fontWeight: 'bold',
                                color: 'white'
                            }
                        },
                        startAngle: -90,
                        endAngle: 90,
                        center: ['50%', '75%']
                    }
                },
                series: [{
                    type: 'pie',
                    name: 'Browser share',
                    innerSize: '50%',
                    data: [
                        ['Firefox',   10.38],
                        ['IE',       56.33],
                        ['Chrome', 24.03],
                        ['Safari',    4.77],
                        ['Opera',     0.91],
                        {
                            name: 'Proprietary or Undetectable',
                            y: 0.2,
                            dataLabels: {
                                enabled: false
                            }
                        }
                    ]
                }]
            });

        });
    </script>

<?php footer(); ?>
