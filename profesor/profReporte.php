<?php require_once("master.php"); cabecera(); ?>
    <div id='wrapper' class='toggled'>
        <?php sideBar(); ?>

        <!-- Page Content -->
        <div id='page-content-wrapper'>
            <?php topBar(); ?>
                <div class='container-fluid'>
                    <div class='row'>
                        <div class='col-md-12 noP fila'>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-7 noP">
                                    <h2 class='titulo2'>Reportes</h2>
                                </div>
                                <div class="col-xs-5 noP">
                                    <img class="imgRecuadro" src="../img/ninos2.png" alt="">
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-12 recuadroInfo sombra noP">
                                    <img class="imgRecuadro" src="../img/ninos.png" alt="">
                                    <h3 class="titulo3">
                                        Niños <br>
                                        <small>542</small>
                                    </h3>
                                    <img class="imgRecuadro2" src="../img/mundo.png" alt="">
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-12 recuadroInfo sombra noP">
                                    <img class="imgRecuadro" src="../img/representantes.png" alt="">
                                    <h3 class="titulo3">
                                        Representantes <br>
                                        <small>854</small>
                                    </h3>
                                    <img class="imgRecuadro2" src="../img/mundo.png" alt="">
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-12 recuadroInfo sombra noP">
                                    <img class="imgRecuadro" src="../img/niveles.png" alt="">
                                    <h3 class="titulo3">
                                        Nivel Asignado: <br>
                                        <small>Ositos</small>
                                    </h3>
                                    <img class="imgRecuadro2" src="../img/mundo.png" alt="">
                                </div>
                            </div>

                        </div>

                        <div class="col-xs-12 noP">
                            <div class="panel panel-default">
                                <!-- Default panel contents -->
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-md-12 addNewBtn">
                                            <!-- Button trigger modal -->
                                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#menu">
                                                Crear Reporte
                                            </button>
                                        </div>


                                        <!-- Modal- Nueva notif -->
                                        <div class="modal fade" id="menu" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                          <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h2 class="modal-title text-center titulo4" id="myModalLabel">Crear nueva notificación <span class="glyphicon glyphicon-send"></span></h2>
                                              </div>
                                              <div class="modal-body">
                                                  <div class="col-md-12 noP">
                                                      <div class="col-md-4">
                                                          <select class="form-control select" name="">
                                                              <option value="1">Enviar por zona</option>
                                                          </select>
                                                      </div>
                                                      <div class="col-md-5">
                                                          <select class="form-control select" name="">
                                                              <option value="1">Lista de afiliados</option>
                                                          </select>
                                                      </div>
                                                      <div class="col-md-3">
                                                          <div class="form-group">
                                                              <input type="checkbox" name="" value="" id="enviarTodos" checked="uncheked">
                                                              <label for="enviarTodos"> Enviar a todos</label>
                                                          </div>

                                                      </div>
                                                  </div>

                                                  <div class="col-md-12 usuariosON">
                                                      <input class="form-control form-control2" type="text" name="" value="" placeholder="Título de la notificación">
                                                      <textarea class="form-control2" name="name" rows="8" cols="80" placeholder="Mensaje"></textarea>
                                                  </div>

                                              </div>
                                              <div class="modal-footer">
                                                <button class="btn btn-success" type="button" name="button">Enviar</button>
                                              </div>
                                            </div>
                                          </div>
                                        </div>


                                    </div>
                                </div>
                                <div class="panel-body">
                                    <button type="button" class="btn btn-default disabled" name="button"><span class="glyphicon glyphicon-trash"></span> Borrar seleccionados</button>
                                </div>

                                <div class="table-responsive">
                                    <!-- Table -->
                                    <table id="notificaciones" class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th>Estado</th>
                                                <th>Fecha</th>
                                                <th>Nombre</th>
                                                <th>Apellido</th>
                                                <th>Importancia</th>
                                                <th>Acciones</th>
                                            </tr>
                                        </thead>
                                        <tr>
                                            <td class="checking"><input type="checkbox" name="" value=""></td>
                                            <td class="checking"></td>
                                            <td>Guardado</td>
                                            <td>03/02/2017</td>
                                            <td>Angela</td>
                                            <td>Padilla</td>
                                            <td>Texto de prueba</td>
                                            <td>
                                                <span class="glyphicon glyphicon-trash iconTable"></span>
                                                <span class="glyphicon glyphicon-eye-open iconTable"></span>
                                                <span class="glyphicon glyphicon-pencil iconTable"></span>                                                 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="checking"><input type="checkbox" name="" value=""></td>
                                            <td class="checking"></td>
                                            <td>Guardado</td>
                                            <td>03/02/2017</td>
                                            <td>Angela</td>
                                            <td>Padilla</td>
                                            <td>Texto de prueba</td>
                                            <td>
                                                <span class="glyphicon glyphicon-trash iconTable"></span>
                                                <span class="glyphicon glyphicon-eye-open iconTable"></span>
                                                <span class="glyphicon glyphicon-pencil iconTable"></span>                                                 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="checking"><input type="checkbox" name="" value=""></td>
                                            <td class="checking"></td>
                                            <td>Guardado</td>
                                            <td>03/02/2017</td>
                                            <td>Angela</td>
                                            <td>Padilla</td>
                                            <td>Texto de prueba</td>
                                            <td>
                                                <span class="glyphicon glyphicon-trash iconTable"></span>
                                                <span class="glyphicon glyphicon-eye-open iconTable"></span>
                                                <span class="glyphicon glyphicon-pencil iconTable"></span>                                                 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="checking"><input type="checkbox" name="" value=""></td>
                                            <td class="checking"></td>
                                            <td>Guardado</td>
                                            <td>03/02/2017</td>
                                            <td>Angela</td>
                                            <td>Padilla</td>
                                            <td>Texto de prueba</t<d>
                                            <td>
                                                <span class="glyphicon glyphicon-trash iconTable"></span>
                                                <span class="glyphicon glyphicon-eye-open iconTable"></span>
                                                <span class="glyphicon glyphicon-pencil iconTable"></span>                                                 
                                            </td>
                                        </tr>

                                    </table>
                                </div>
                                <div class="panel-body">
                                    <button type="button" class="btn btn-default disabled" name="button"><span class="glyphicon glyphicon-trash"></span> Borrar seleccionados</button>
                                </div>
                            </div>
                        </div>


                    </div>
                </div><!--container-->
        </div>
        <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->


    <script>
        $(document).ready(function () {
            // Seccion active (MENU)
            $(document).ready(function () {
                $('#notificacion').addClass('activo');
                $('#notificacionli').addClass('activoli');
            });

            $(document).ready(function() {
                $('#notificaciones').DataTable();
            } );

        });
    </script>

<?php footer(); ?>
