<?php require_once("master.php"); cabecera(); ?>
    <div id='wrapper' class='toggled'>
        <?php sideBar(); ?>

        <!-- Page Content -->
        <div id='page-content-wrapper'>
            <?php topBar(); ?>
                <div class='container-fluid'>
                    <div class='row'>
                        <div class='col-xs-12'>
                            <div class='col-md-6 fila '>
                                    <div class=" col-md-6 col-xs-7 noP">
                                        <h2 class='titulo2'>Perfil del Niño</h2>
                                    </div>
                                    <div class="col-md-6 col-xs-5 noP">
                                        <img class="imgTtitulo" src="../img/ninos2.png" alt="">
                                    </div>
                            </div>

                            <div class='col-md-6 col-xs-12 noP'>
                                 <p><span class="glyphicon glyphicon-exclamation-sign text-danger"></span> Ahora usted puede editar los campos de información de cada niño de forma individual</p>
                                 <p><span class="glyphicon glyphicon-minus"></span>Agregar Fotos</p>
                                 <p><span class="glyphicon glyphicon-minus"></span>Actualice Datos de Contácto</p>
                                 <p><span class="glyphicon glyphicon-minus"></span>Cambie la información de Profesor y Nivel Asignado a Cada Niño</p>
                                 <p><span class="glyphicon glyphicon-minus"></span>Agregue nuevos representantes para el niño</p>
                            </div>
                        </div>

                            <div class='col-md-6 col-xs-12 recuadrop  '>
                                <div class="col-xs-12 ficha">
                                    <img class="imgRecuadro img-circle" src="../img/ninos.png" alt="">
                                    <h4>Ficha Personal del Niño</h4>
                                    <p>Nombre y Apellido</p>
                                    <p>Edad</p>
                                    <img class="img2" src="../img/girl.png" alt="">
                                    <span class="glyphicon glyphicon-pencil IconoLapiz"></span>
                                </div>
                            </div>
                        <form role="form">
                            <div class='col-md-6 col-xs-12 recuadrop' >
                                    <div class='col-xs-12 col-md-6 text-center'>
                                        <span>Usa pañal:</span>
                                        <div class='form-group radiosbebe'>
                                            SI <input type="radio" name="optradio">
                                            <input type="radio" name="optradio"> NO
                                        </div>
                                    </div>

                                    <div class='col-xs-12 col-md-6 text-center'>
                                        <span>Va solo al baño:</span>
                                        <div class='form-group radiosbebe'>
                                            SI <input type="radio" name="optradio">
                                            <input type="radio" name="optradio"> NO
                                        </div>
                                    </div>

                                    <div class='col-xs-12 col-md-6 text-center'>
                                        <span>Toma Tetero:</span>
                                        <div class='form-group radiosbebe'>
                                            SI <input type="radio" name="optradio">
                                            <input type="radio" name="optradio"> NO
                                        </div>
                                    </div>

                                    <div class='col-xs-12 col-md-6 text-center'>
                                        <span>Come solo:</span>
                                        <div class='form-group radiosbebe'>
                                            SI <input type="radio" name="optradio">
                                            <input type="radio" name="optradio"> NO
                                        </div>
                                    </div>

                                </div>

                            <div class='col-md-6 col-xs-12 recuadrop'>
                                <div class="col-xs-12 ficha">
                                    <img class="imgRecuadro img-circle" src="../img/ninos.png" alt="">
                                    <h4>Representante1</h4>
                                    <p>Nombre y Apellido</p>
                                    <p>Número de Teléfono</p>
                                    <img class="img2" src="../img/girl.png" alt="">
                                    <span class="glyphicon glyphicon-pencil IconoLapiz"></span>
                                </div>
                            </div>
                        </form>
                            <div class='col-md-6 col-xs-12 recuadrop'>
                                <div class="col-xs-12 ficha">
                                    <img class="imgRecuadro img-circle" src="../img/ninos.png" alt="">
                                    <h4>Representante2</h4>
                                    <p>Nombre y Apellido</p>
                                     <p>Número de Teléfono</p>
                                    <img class="img2" src="../img/girl.png" alt="">
                                    <span class="glyphicon glyphicon-pencil IconoLapiz" ></span>
                                </div>
                            </div>
                            <div class='col-xs-12 text-center'>
                                <a class="btn btn-info " href="dirRepresentanteNuevo.php">Ver Representantes Asignados</a>
                            </div>

                            <div  class='col-md-9 col-md-offset-2'>
                                <div class='col-md-6'>
                                    <div class="col-xs-7 ficha">
                                        <h4>Profesor Asignado</h4>
                                        <p>Nombre del Profesor</p>
                                     </div>
                                     <div class="col-xs-5">
                                            <img class="imgRecuadro img-circle" src="../img/profesores2.png" alt="">
                                    </div>
                                </div>
                                <div  class='col-md-6'>
                                    <div class="col-xs-7 ficha">
                                        <h4>Nivel Asignado</h4>
                                        <p>Sección/Salón</p>
                                     </div>
                                     <div class="col-xs-5">
                                      
                                         <img class="imgRecuadro img-circle" src="../img/niveles.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-xs-12 text-center">
                                    <a class="btn btn-success" href="#">Guardar Cambios</a>
                                    <a class="btn btn-warning" href="dirProfesores.php">Volver</a>
                            </div>

                        </div>
                    </div>
                </div><!--container-->
        </div>
        <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->


    <script>
        $(document).ready(function () {
            // Seccion active (MENU)
            $(document).ready(function () {
                $('#ninos').addClass('activo');
                $('#ninosli').addClass('activoli');
            });

            $(document).ready(function() {
                $('#profe').DataTable();
            } );

        });
    </script>

<?php footer(); ?>
