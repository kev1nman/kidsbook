<?php require_once("master.php"); cabecera(); ?>
    <div id='wrapper' class='toggled'>
        <?php sideBar(); ?>

        <!-- Page Content -->
        <div id='page-content-wrapper'>
            <?php topBar(); ?>
                <div class='container-fluid'>
                    <div class='row'>
                        <div class='col-md-12 noP fila'>
                            <div class="col-md-6 col-lg-4 recuadro">
                                <div class="col-xs-8 noP">
                                    <h2 class='titulo2'>Agregar Reporte</h2>
                                </div>
                                <div class="col-xs-4 noP">
                                    <img class="imgTtitulo" src="../img/notificacion.png" alt="">
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-4 recuadro">
                                <div class="col-xs-12 noP">
                                    <form>
                                     <div class="form-group">
                                         <select class="form-control">
                                             <option>Nombre del Niño</option>
                                             <option>Jhan</option>
                                             <option>Mario</option>
                                             <option>Juana</option>
                                        </select>
                                    </div>
                                   </form>
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-4 recuadro text-center ">
                                <div class=" col-xs-12 ">                 
                                    <h3 class="titulo3">
                                        Estado del Reporte 
                                        <div class="circulo"></div><p>no enviado</p>

                                      </h3> 
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 noP">                            
                            <div class='col-md-4 col-xs-12 recuadrop  '>
                                <div class="col-xs-12 ficha">
                                    <img class="imgRecuadro img-circle" src="../img/ninos.png" alt="">
                                    <h4>Ficha Personal del Niño</h4>
                                    <p>Nombre y Apellido</p>
                                    <p>Edad</p>
                                    <img class="img2" src="../img/girl.png" alt="">
                                </div>
                            </div>

                            <div class='col-md-4 col-xs-12 recuadrop  '>
                                <div class="col-xs-12 ficha">
                                    <img class="imgRecuadro img-circle" src="../img/ninos.png" alt="">
                                    <h4>Representante1</h4>
                                    <p>Nombre y Apellido</p>
                                    <p>Número de teléfono</p>            
                                </div>
                            </div>

                            <div class='col-md-4 col-xs-12 recuadrop  '>
                                <div class="col-xs-12 ficha">
                                    <img class="imgRecuadro img-circle" src="../img/ninos.png" alt="">
                                    <h4>Representante2</h4>
                                    <p>Nombre y Apellido</p>
                                    <p>Número de teléfono</p>
                                </div>
                            </div>
                        </div>

                         <div class="col-xs-12 noP">  
                             <div class='col-md-5 col-xs-12 recuadrop '>
                                  <div class="col-xs-1">
                                        <h5>Desayuno</h5>
                                  </div>
                                  <div class="col-xs-2 ">
                                        <img class="imgRecuadro" src="../img/lollipop.png" alt="">
                                  </div>
                                  <div class="col-xs-2">
                                        <img class="imgRecuadro" src="../img/lollipop.png" alt="">
                                  </div>
                                  <div class="col-xs-2">
                                        <img class="imgRecuadro" src="../img/lollipop.png" alt="">
                                  </div>
                                  <div class="col-xs-2">
                                        <img class="imgRecuadro" src="../img/lollipop.png" alt="">
                                  </div>
                                  <div class="col-xs-2">
                                        <img class="imgRecuadro" src="../img/lollipop.png" alt="">
                                  </div>       
                            </div>

                        <div class='col-md-7 col-xs-12 recuadrop '>
                                <form>
                                    <div class='col-xs-12 col-md-4 noP'>
                                        <img class="imgRecuadro" src="../img/lollipop.png" alt="">

                                        <div class='form-group'>
                                            <div class="col-xs-4">
                                                <span>Cantidad</span> <input class="form-control" type="text" >
                                            </div>
                                        </div>
                                    </div>
                                     
                                     <div class='col-xs-12 col-md-4 noP'>
                                        <img class="imgRecuadro" src="../img/lollipop.png" alt="">
                                        <div class='form-group'>
                                            Durmio <input type="radio" name="optradio">
                                        </div>
                                    </div>

                                     <div class='col-xs-12 col-md-4 noP'>
                                        <img class="imgRecuadro" src="../img/lollipop.png" alt="">
                                        <span>Cuanto Tiempo</span>
                                        <div class='form-group'>
                                            <div class="col-xs-3 col-md-4">
                                                <input class="form-control" type="text" >
                                            </div>
                                        </div>
                                     </div>
                                </form>
                         </div>
                    </div>

                     <div class="col-xs-12 noP">  
                             <div class='col-md-6 recuadrop '>
                                    <div class="col-xs-12 col-md-6">
                                        <img class="imgRecuadro" src="../img/lollipop.png" alt="">
                                        <div class='form-group'>
                                            Fue al baño <input type="radio" name="optradio">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <img class="imgRecuadro" src="../img/lollipop.png" alt="">
                                        <div class='form-group'>
                                            <div class="col-xs-3 col-md-3">
                                               Liquido N° Veces
                                            </div>
                                            <div class="col-xs-3 col-md-4">
                                               <input class="form-control" type="text" >
                                            </div>
                                        </div>
                                     </div>
                                 </div>
                                  <div class='col-md-6 recuadrop '>
                                    <div class="col-xs-12 col-md-6">
                                        <img class="imgRecuadro" src="../img/lollipop.png" alt="">
                                        <div class='form-group'>
                                            <div class="col-xs-6 col-md-3">
                                               Sólido N° Veces
                                            </div>
                                            <div class="col-xs-6 col-md-4">
                                               <input class="form-control" type="text" >
                                            </div>
                                        </div>
                                    </div>
                                     <div class="col-xs-12 col-md-6">
                                        <img class="imgRecuadro" src="../img/lollipop.png" alt="">
                                        <div class='form-group'>
                                            <div class="col-xs-6 col-md-3">
                                               Total pañales usados
                                            </div>
                                            <div class="col-xs-6 col-md-4">
                                               <input class="form-control" type="text" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        
                        <div class='col-xs-12 recuadrop '>
    
                                <div class="col-xs-6">
                                    <form>
                                         <div class='col-xs-12 noP'>
                                            <div class='form-group'>
                                                <textarea class="form-control" rows="3">Observaciones y asignaciones importantes</textarea>
                                            </div>     
                                        </div>
                                    </form>
                                </div>
                            
                            
                                <div class="col-xs-6">
                                    <form>
                                         <div class='col-xs-12 noP'>
                                            <div class='form-group'>
                                                <textarea class="form-control" rows="3">Mensaje</textarea>
                                            </div>     
                                        </div>
                                    </form>
                                </div>
                        
                    </div>
                
                  <div class='col-xs-12 col-md-5 noP'>  
                            <div class="col-xs-12 col-md-2">
                                     <img class="imgRecuadro" src="../img/premio.png" alt="">
                            </div>
                                <div class="col-xs-12 col-md-4">
                                     <a class="btn btn-info" href="#">Premiar Niño</a>
                                </div>

                                <div class="col-xs-12 col-md-2">
                                     <img class="imgRecuadro" src="../img/bear.png" alt="">
                                </div>
                                <div class="col-xs-12 col-md-4">
                                     <a class="btn btn btn-warning" href="#">Indicar estado de ánimo</a>
                                </div> 
                        </div>

                        <div class='col-xs-12 col-md-7 text-center noP'>
                             <div class="col-xs-12 col-md-4">
                                 <a class="btn btn-success" href="#">Enviar Reporte</a>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <a class="btn btn-warning" href="#">Guardar Reporte</a>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                 <a class="btn btn-danger" href="#">Cancelar</a>
                            </div>            
                        </div>
                    
                    </div><!--row-->
                </div><!--container-->
        </div>
        <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->


    <script>
        $(document).ready(function () {
            // Seccion active (MENU)
            $(document).ready(function () {
                $('#notificacion').addClass('activo');
                $('#notificacionli').addClass('activoli');
            });

            $(document).ready(function() {
                $('#notificaciones').DataTable();
            } );

        });
    </script>

<?php footer(); ?>