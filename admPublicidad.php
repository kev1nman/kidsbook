<?php require_once("master.php"); cabecera(); ?>
    <div id='wrapper' class='toggled'>
        <?php sideBar(); ?>

        <!-- Page Content -->
        <div id='page-content-wrapper'>
            <?php topBar(); ?>
                <div class='container-fluid'>
                    <div class='row'>
                        <div class='col-md-12 noP fila'>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-7 noP">
                                    <h2 class='titulo2'>Publicidad</h2>
                                </div>
                                <div class="col-xs-5 noP">
                                    <img class="imgTtitulo" src="img/publicid.png" alt="">
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-12 recuadroInfo sombra noP">
                                    <img class="imgRecuadro" src="img/ninos.png" alt="">
                                    <h3 class="titulo3">
                                        Niños <br>
                                        <small>542</small>
                                    </h3>
                                    <img class="imgRecuadro2" src="img/mundo.png" alt="">
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-12 recuadroInfo sombra noP">
                                    <img class="imgRecuadro" src="img/representantes.png" alt="">
                                    <h3 class="titulo3">
                                        Representantes <br>
                                        <small>854</small>
                                    </h3>
                                    <img class="imgRecuadro2" src="img/mundo.png" alt="">
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3 recuadro">
                                <div class="col-xs-12 recuadroInfo sombra noP">
                                    <img class="imgRecuadro" src="img/colegios.png" alt="">
                                    <h3 class="titulo3">
                                        Colegios <br>
                                        <small>19</small>
                                    </h3>
                                    <img class="imgRecuadro2" src="img/mundo.png" alt="">
                                </div>
                            </div>

                        </div>

                        <div class="col-xs-12 noP">
                            <div class="panel panel-default">
                                <!-- Default panel contents -->
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-md-4 addNewBtn">
                                            <a class="btn btn-success" href="admPublicidadNuevo.php">Activar publicidad</a>
                                        </div>
                                        <div class='col-xs-10 col-sm-6 boxSearch boxSearch2'>
                                            <div class='input-group'>
                                                <span class='input-group-btn'>
                                                    <button class='btn btn-default btnSearch' type='button'><span class='glyphicon glyphicon-search'></span></button>
                                                </span>
                                                <input type='text' class='form-control searchMenu2' placeholder='Buscar colegios...'>
                                            </div>
                                            <!-- /input-group -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <div class="col-md-3 text-center">
                                    <div class="fondoPublicidad">
                                        <a href="#">
                                            <img class="imgPublicidad2" src="img/banner1.jpg" alt="">
                                        </a>
                                        <div class="col-xs-12">
                                            <div class="btnPublicidad">
                                                <span class="glyphicon glyphicon-trash iconTable"></span>
                                                <span class="glyphicon glyphicon-eye-open iconTable"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-3 text-center">
                                    <div class="fondoPublicidad">
                                        <a href="#">
                                            <img class="imgPublicidad2" src="img/banner1.jpg" alt="">
                                        </a>
                                    </div>
                                </div>


                                <div class="col-md-3 text-center">
                                    <div class="fondoPublicidad">
                                        <a href="#">
                                            <img class="imgPublicidad2" src="img/banner1.jpg" alt="">
                                        </a>
                                        <div class="col-xs-12">
                                            <div class="btnPublicidad">
                                                <span class="glyphicon glyphicon-trash iconTable"></span>
                                                <span class="glyphicon glyphicon-eye-open iconTable"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 text-center">
                                    <div class="fondoPublicidad">
                                        <a href="#">
                                            <img class="imgPublicidad2" src="img/banner1.jpg" alt="">
                                        </a>
                                        <div class="col-xs-12">
                                            <div class="btnPublicidad">
                                                <span class="glyphicon glyphicon-trash iconTable"></span>
                                                <span class="glyphicon glyphicon-eye-open iconTable"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                            </div>
                        </div>


                    </div>
                </div><!--container-->
        </div>
        <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->


    <script>
        $(document).ready(function () {
            // Seccion active (MENU)
            $(document).ready(function () {
                $('#publicid').addClass('activo');
                $('#publicidli').addClass('activoli');
            });

        });
    </script>

<?php footer(); ?>
